-- 
-- packages/intranet-collmex/sql/postgresql/upgrade/upgrade-4.0.5.0.1-4.0.5.0.2.sql
-- 
-- Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- @author <yourname> (<your email>)
-- @creation-date 2021-12-10 
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-collmex/sql/postgresql/upgrade/upgrade-5.0.0.1.4-5.0.0.1.5.sql','');
CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'collmex_accbal';
    IF 0 != v_count THEN return 0; END IF;
    
    create table collmex_accbal (
        bal_date date,
        account_id integer,
        amount numeric
    );

    alter table collmex_accbal add primary key (bal_date, account_id);
    return 1;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'collmex_accounts';
    IF 0 != v_count THEN return 0; END IF;
    
    create table collmex_accounts (
        account_id integer primary key not null,
        account_name text
    );
    return 1;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();