SELECT acs_log__debug('/packages/intranet-collmex/sql/postgresql/upgrade/upgrade-5.0.0.1.6-5.0.0.1.7.sql','');
alter table collmex_accdoc drop constraint collmex_accdoc_invoice_fk;
alter table collmex_accdoc add constraint collmex_accdoc_invoice_fk foreign key (invoice_id) references im_invoices on delete cascade;

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''collmex_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''collmex_id'',
                ''Collmex Id'',
                ''textbox_small'',
                ''string'',
                ''f'',
                10,
                ''f'',
                ''im_companies''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''company'', 10, ''plain'');
        END IF;

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();
