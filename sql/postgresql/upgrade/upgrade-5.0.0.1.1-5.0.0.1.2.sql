-- 
-- packages/intranet-collmex/sql/postgresql/upgrade/upgrade-4.0.5.0.1-4.0.5.0.2.sql
-- 
-- Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- @author <yourname> (<your email>)
-- @creation-date 2012-01-27
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-collmex/sql/postgresql/upgrade/upgrade-5.0.0.1.1-5.0.0.1.2.sql','');
CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'collmex_accdoc';
    IF 0 != v_count THEN return 0; END IF;
    
    create table collmex_accdoc (
        invoice_id     integer not null
            constraint  collmex_accdoc_invoice_fk
            references im_invoices,
        booking_year    integer,
        booking_id      integer,
        booking_date    date,
        creation_date   date,
        booking_text    varchar(255),
        account_id      integer,
        cost_center     varchar(255),
        internal_note   varchar(255)
    );
    return 1;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();