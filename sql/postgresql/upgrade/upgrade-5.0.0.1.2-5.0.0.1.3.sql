SELECT acs_log__debug('/packages/intranet-collmex/sql/postgresql/upgrade/upgrade-5.0.0.1.2-5.0.0.1.3.sql','');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from  information_schema.columns
where lower(table_name) = 'collmex_accdoc' and column_name = 'booking_pos';
    IF 0 != v_count THEN return 0;
    ElSE 
    alter table collmex_accdoc add column booking_pos integer default 1;
    return 1;
    END IF;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from  information_schema.columns
where lower(table_name) = 'collmex_accdoc' and column_name = 'account_name';
    IF 0 != v_count THEN return 0; END IF;
    
    alter table collmex_accdoc add column account_name varchar(255);
    return 1;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from  information_schema.columns
where lower(table_name) = 'collmex_accdoc' and column_name = 'booking_amount';
    IF 0 != v_count THEN return 0; END IF;
    
    alter table collmex_accdoc add column booking_amount numeric;
    return 1;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from  information_schema.columns
where lower(table_name) = 'collmex_accdoc' and column_name = 'company_collmex_id';
    IF 0 != v_count THEN return 0; END IF;
    
    alter table collmex_accdoc add column company_collmex_id integer;
    return 1;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();