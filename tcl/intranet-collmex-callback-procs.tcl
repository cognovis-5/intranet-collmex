# packages/intranet-collmex/tcl/intranet-collmex-callback-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
# 

ad_library {
    
    Procedures for the callbacks where we hook into
    
    @author <yourname> (<your email>)
    @creation-date 2012-01-06
    @cvs-id $Id$
}


ad_proc -public -callback im_company_after_update -impl intranet-collmex_update_company {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Update or create the company in Collmex
} {
    # Find out if this is a customer
    set company_id $object_id
    set company_type_id $type_id
    set csv_line ""
    set collmex_id ""

    if {[lsearch [im_sub_categories [im_company_type_customer]] $company_type_id] >-1} {
        set csv_line [intranet_collmex::CMXKND -customer_id $company_id -error_var "errors"]
    }

    if {[lsearch [im_sub_categories [im_company_type_provider]] $company_type_id] >-1} {
        set csv_line [intranet_collmex::CMXLIF -provider_id $company_id -error_var "errors"]
    }
    if {$csv_line ne ""} {
        set collmex_id [intranet_collmex::update_company -company_id $company_id]
    }
    return $collmex_id
}

ad_proc -public -callback im_company_after_create -impl intranet-collmex_create_company {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Create the company in Collmex
} {
        # Find out if this is a customer
    set company_id $object_id
    set company_type_id $type_id
    set csv_line ""

    if {[lsearch [im_sub_categories [im_company_type_customer]] $company_type_id] >-1} {
        set csv_line [intranet_collmex::CMXKND -customer_id $company_id]
    }

    if {[lsearch [im_sub_categories [im_company_type_provider]] $company_type_id] >-1} {
        set csv_line [intranet_collmex::CMXLIF -provider_id $company_id]
    }
    if {$csv_line ne ""} {
        set collmex_id "[intranet_collmex::update_company -company_id $company_id]"
        if {$collmex_id ne ""} {
            return "Created company $company_id in Collmex: $collmex_id"
        } 
    }
}

ad_proc -public -callback im_invoice_after_create -impl zz_intranet-collmex_invoice_handling {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    This is the complex handle all types of invoice changes function for collmex
} {
    set relevant_status_ids [parameter::get_from_package_key -package_key "intranet-collmex" -parameter "RelevantCostStatus"]
    set current_status_id [db_string status "select cost_status_id from im_costs where cost_id = :object_id" -default ""]
    if {[lsearch $relevant_status_ids $current_status_id]>-1} {
        if {[lsearch [im_sub_categories 3700] $type_id] >-1} {
            # Customer Invoice                        
            set collmex_ids [intranet_collmex::update_customer_invoice -invoice_id $object_id]                                                                                          
            cog_log Notice "Creating invoice in Collmex [im_name_from_id $status_id] [im_name_from_id $type_id]:: $collmex_ids"
            return "Created collmex invoice: $collmex_ids"
        }

        if {[lsearch [im_sub_categories 3704] $type_id] >-1} {
            # Provider Bill  
            set collmex_ids [intranet_collmex::update_provider_bill -invoice_id $object_id]                                                                                                                                                                                                         
            cog_log Notice "Creating bill in Collmex [im_name_from_id $status_id] [im_name_from_id $type_id]:: $collmex_ids"
            return "Created collmex bill: $collmex_ids"
        }
    } else {
        return "Not relevant: $current_status_id"
    }
}

ad_proc -public -callback im_invoice_after_update -impl zz_intranet-collmex_invoice_handling {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    After updating an invoice, update the data in collmex as well. Behaves exactly like after create.
} {
    return [callback::im_invoice_after_create::impl::zz_intranet-collmex_invoice_handling -object_id $object_id -status_id $status_id -type_id $type_id]
}

ad_proc -public -callback im_invoice_after_update -impl zz_intranet-collmex_storno {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    This is the complex handle all types of invoice changes function for collmex when the invoice is set to deleted or rejected
} {
    #set relevant_status_ids [list [im_cost_status_deleted] [im_cost_status_rejected]]
    set relevant_status_ids ""
    set current_status_id [db_string status "select cost_status_id from im_costs where cost_id = :object_id" -default ""]
    if {[lsearch $relevant_status_ids $current_status_id]>-1} {
        if {[lsearch [im_sub_categories 3700] $type_id] >-1} {
            # Customer Invoice
            return "Deleting invoice in Collmex:: [intranet_collmex::update_customer_invoice -invoice_id $object_id -storno]"
        } 
        
        if {[lsearch [im_sub_categories 3704] $type_id] >-1} {
            # Provider Bill
            return "Deleting bill in Collmex:: [intranet_collmex::update_provider_bill -invoice_id $object_id -storno]"
        }
    }
}

ad_proc -public -callback im_invoice_before_nuke -impl zz_intranet-collmex_invoice_handling {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    This is the complex handle all types of invoice changes function for collmex when they are deleted
} {
    
    if {[lsearch [im_sub_categories 3700] $type_id] >-1} {
        # Customer Invoice
        set collmex_ids [intranet_collmex::update_customer_invoice -invoice_id $object_id -storno]                                                                                         
        cog_log Notice "Deleting invoice in Collmex:: $collmex_ids"
        return "Storno $collmex_ids for $object_id"
    } 
    
    if {[lsearch [im_sub_categories 3704] $type_id] >-1} {
        # Provider Bill
        set collmex_ids [intranet_collmex::update_provider_bill -invoice_id $object_id -storno] 
        cog_log Notice "Deleting bill in Collmex:: $collmex_ids"
        return "Storno $collmex_ids for $object_id"
    }
}

ad_proc -public -callback cog_rest::object_return_element -impl collmex_company {
    -proc_name
    -object_class
} {
    append collmex_id to company using an injection into json_object

    This does not require manipulation of return elements as the -init.tcl includes code to extend the company object

} {
	if {$proc_name eq "cog_rest::get::companies"} {
		upvar 2 company_id company_id
        upvar 2 collmex_id collmex_id
        set collmex_id [db_string collmex_id "select collmex_id from im_companies where company_id = :company_id" -default ""]
	}
} 

ad_proc -public -callback intranet_collmex::error {
    -object_id
    -message
} {
    Called when an error occured
        
    @param object_id Object which resulted in the error
    @param message Message with the error 
} - 
