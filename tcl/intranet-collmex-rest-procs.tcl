ad_library {
    REST Procedures for collmex objects
    @author malte.sussdorff@cognovis.de
}

namespace eval cog_rest::json_object {
    ad_proc collmex_accdoc {} {
        @return invoice object im_invoice Invoice to which this booking relates
        @return booking_year integer Year of the booking
        @return booking_id integer ID of the booking in the given year
        @return booking_pos integer Position of the line item in the booking
        @return booking_amount number Amount of the booking
        @return creation_date date Date when the booking was created
        @return booking_date date Date when the booking was effective
        @return account named_id Account which was used for this booking line
        @return internal_note string Note with the booking
        @return company_collmex_id integer ID for the company associated with the booking
        @return company object im_company Company associated with the booking if found
    } -

    ad_proc collmex_accbal {} {
        @return bal_date date Date of the balance
        @return account named_id Account of the amount in the bal_date
        @return amount number How big was the balance for the account on the given date
    } - 
}

namespace eval cog_rest::get {
    ad_proc collmex_accdoc {
        {-invoice_id ""}
        {-company_id ""}
        -rest_user_id:required
    } {
        Returns an array of bookings

        @param invoice_id object im_invoice::read Invoice to which this booking relates
        @param company_id object im_company::admin Company for which we need to get the bookings
        @return accdocs json_array collmex_accdoc bookings in collmex
    } {
        set accdocs [list]
        set where_clause_list [list]
        if {$invoice_id ne ""} {
            lappend where_clause_list "invoice_id = :invoice_id"
        }
        if {$company_id ne ""} {
            set company_collmex_id [db_string company_collmex "select company_collmex_id from im_companies where company_id = :company_id" -default ""]
            if {$company_collmex_id ne ""} {
                lappend where_clause_list "collmex_id = :company_collmex_id"
            }
        }

        if {[llength $where_clause_list] >0} {
            lappend where_clause_list "c.collmex_id = a.company_collmex_id"
            db_foreach accdoc "select invoice_id, booking_year, booking_id, booking_pos, creation_date::date, booking_date::date, booking_text,
                account_id, account_name, cost_center, internal_note, a.company_collmex_id, booking_amount, 
                company_id, company_name 
                from im_companies c, collmex_accdoc a
                where [join $where_clause_list " and "]" {
                    lappend accdocs [cog_rest::json_object]
                }
                return [cog_rest::json_response]
        } else {
            cog_rest::error -http_status 400 -message "You need to provide company or invoice and need to have permissions on the company"
        }
    }
}

namespace eval cog_rest::post {
    ad_proc -public import_company_to_collmex {
        -company_id:required
        -rest_user_id:required
    } {
        Imports the company to collmex

        @param company_id object im_company::write Company which we want to import to collmex. 

        @return company json_object company Object of the Company which we updated.
    } {
        set collmex_id [intranet_collmex::update_company -company_id $company_id]
        if {$collmex_id eq ""} {
            cog_rest::error -http_status 500 -message "We could not add the company to collmex. Most likely the master data is insufficient"
        }
        
        set company [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::companies -company_id $company_id -rest_user_id $rest_user_id]]
        
        return [cog_rest::json_response]
    }
}