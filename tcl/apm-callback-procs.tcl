ad_library {
    APM callback procedures.

    @creation-date 2021-12-21
    @author malte.sussdorff@cognovis.de
}

namespace eval intranet-collmex::apm {}

ad_proc -public intranet-collmex::apm::after_upgrade {
    {-from_version_name:required}
    {-to_version_name:required}
} {
    After upgrade callback.
} {
    apm_upgrade_logic \
        -from_version_name $from_version_name \
        -to_version_name $to_version_name \
        -spec {
            5.0.0.1.5 5.0.0.1.6 {
                intranet-collmex::update_accounts
            }
        }
}