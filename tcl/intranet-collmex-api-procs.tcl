# packages/intranet-collmex/tcl/intranet-collmex-api-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

ad_library {
    
    Procedures to generate or parse Satzarten from collmex
    
    @author <yourname> (<your email>)
    @creation-date 2012-01-04
    @cvs-id $Id$
}

namespace eval intranet_collmex {

    ad_proc -public CMXKND {
        -customer_id
        {-customer_contact_id ""}
        {-error_var ""}
    } {
        Generates a customer record for sending to collmex

        @param customer_id Company we want to send over to collmex
        @param customer_contact_id Company contact we want to send. Defaults to accounting_contact_id, then primary_contact_id. Null otherwise

        use field description from http://www.collmex.de/cgi-bin/cgi.exe?1005,1,help,daten_importieren_kunde
    } {
        if {$error_var ne ""} {
            upvar $error_var errors
        } else {
            set errors ""
        }

        if {$customer_contact_id eq ""} {
            set customer_contact_id [db_string accounting "select coalesce(accounting_contact_id,primary_contact_id) from im_companies where company_id = :customer_id" -default ""]
        }

        if {$customer_contact_id eq ""} {
            set errors "Could not find an accounting or primary contact for the company [im_name_from_id $customer_id]."
            intranet_collmex::error -object_id $customer_id -message $errors
            return ""
        } else {
            db_1row contact_info {
                select * from cc_users where user_id = :customer_contact_id
            }
        }

        set csv [new_CkCsv]
        CkCsv_put_Delimiter $csv ";"

        db_1row customer_info {
            select *
                from im_offices o, im_companies c
            where c.main_office_id = o.office_id
            and c.company_id = :customer_id
        }

        if {$address_city eq ""} {
            set errors "Company [im_name_from_id $customer_id] does not have a city"
            intranet_collmex::error -object_id $customer_id -message $errors
            return ""
        }

        # Translation of the country code
        switch $address_country_code {
            "uk" {set address_country_code gb}
            ""   {set address_country_code de} ; # default country code germany
        }
        
        CkCsv_SetCell $csv 0 0 "CMXKND"    

        if {[exists_and_not_null collmex_id]} {
            CkCsv_SetCell $csv 0 1 $collmex_id    
        }

        set locale [lang::user::site_wide_locale -user_id $customer_contact_id]
        CkCsv_SetCell $csv 0 2 1 ; # Firma Nr (internal)
        CkCsv_SetCell $csv 0 3 [string range [im_category_from_id -current_user_id $customer_contact_id -locale $locale $salutation_id] 0 9] ; # Anrede
        CkCsv_SetCell $csv 0 4 [string range $position 0 9] ; # Title
        CkCsv_SetCell $csv 0 5 [string range $first_names 0 39] ; # Vorname
        CkCsv_SetCell $csv 0 6 [string range $last_name 0 39];# Name
        CkCsv_SetCell $csv 0 7 [string range $company_name 0 79] ; # Firma
        CkCsv_SetCell $csv 0 8 [string range $job_title 0 39] ; # Abteilung
        
        if {$address_line2 ne ""} {
            append address_line1 "\n $address_line2"
        }

        CkCsv_SetCell $csv 0 9 [string range $address_line1 0 39] ; # Straße
        
        CkCsv_SetCell $csv 0 10 [string range $address_postal_code 0 9] ; # PLZ
        CkCsv_SetCell $csv 0 11 [string range $address_city 0 39] ; # Ort
        CkCsv_SetCell $csv 0 12 [string range $note 0 1023] ; # Bemerkung

        switch $company_status_id {
            49 {
                # Deleted
                set company_status 3
            }
            47 - 48 - 50 {
                # Inactive
                set company_status 1
            }
            default {
                set company_status 0
            }
        }
        CkCsv_SetCell $csv 0 13 $company_status ; # Inaktiv
        CkCsv_SetCell $csv 0 14 [string range $address_country_code 0 1] ; # Land
        CkCsv_SetCell $csv 0 15 [string range $phone 0 39] ; # Telefon
        CkCsv_SetCell $csv 0 16 [string range $fax 0 39] ; # Telefax
        CkCsv_SetCell $csv 0 17 [string range $email 0 49] ; # E-Mail
        CkCsv_SetCell $csv 0 18 [string range $bank_account_nr 0 19] ; # Kontonr
        CkCsv_SetCell $csv 0 19 [string range $bank_routing_nr 0 19] ; # Blz
        CkCsv_SetCell $csv 0 20 [string range $iban 0 39] ; # Iban
        CkCsv_SetCell $csv 0 21 [string range $bic 0 19] ; # Bic
        CkCsv_SetCell $csv 0 22 [string range $bank_name 0 39] ; # Bankname
        CkCsv_SetCell $csv 0 23 [string range $tax_number 0 19] ; # Steuernummer
        CkCsv_SetCell $csv 0 24 [string range $vat_number 0 19] ; # USt.IdNr
        CkCsv_SetCell $csv 0 25 "6" ; # Zahlungsbedingung - als Zahl codiert, beginnend mit 0 für 30T ohne Abzug, wie im Programm unter "Einstellungen" 
        CkCsv_SetCell $csv 0 26 "" ; # Rabattgruppe - interne Nummer der Rabattgruppe, wie sie bei der Pflege der Rabattgruppe in der ersten Spalte angezeigt wird. 
        CkCsv_SetCell $csv 0 27 "" ; # Lieferbedingung - International genormte INCOTERMS 
        CkCsv_SetCell $csv 0 28 "" ; # Lieferbedingung Zusatz - Ort der Lieferbedingung 
        CkCsv_SetCell $csv 0 29 "1" ; # Ausgabemedium - Als Zahl codiertes Ausgabemedium: 0 = Druck, 1 = E-Mail, 2 = Fax, 3 = Brief. Standard ist E-Mail. 
        CkCsv_SetCell $csv 0 30 "" ; # Kontoinhaber - nur erforderlich falls abweichend 
        CkCsv_SetCell $csv 0 31 "" ; # Adressgruppe - interne Nummer der Adressgruppe, wie sie bei der Pflege der Adressgruppen in der ersten Spalte angezeigt wird. Soll der Kunde mehreren Adressgruppen zugeordnet werden, sind diese jeweils durch ein Komma zu trennen (z.B. 1,2,3). Der Wert '0' entfernt den Kunden aus allen Adressgruppen. Wird nichts angegeben, bleibt die Zuordnung des Kunden zu den Gruppen unverändert. 
        CkCsv_SetCell $csv 0 32 "" ; # eBay-Mitgliedsname - Der eBay-Mitgliedsname des Kunden 
        CkCsv_SetCell $csv 0 33 "" ; # Preisgruppe - interne Nummer der Preisgruppe, wie sie bei der Pflege der Preisgruppe in der ersten Spalte angezeigt wird. 
        CkCsv_SetCell $csv 0 34 "EUR" ; # Währung (ISO-Codes)
        CkCsv_SetCell $csv 0 35 "" ; # Vermittler - Mitarbeiter Nummer des Vermittlers für die Provisionsabrechnung. 
        CkCsv_SetCell $csv 0 36 "" ; # Kostenstelle
        CkCsv_SetCell $csv 0 37 "" ; # Wiedervorlage am
        CkCsv_SetCell $csv 0 38 "" ; # Liefersperre - 1 = Liefersperre 
        CkCsv_SetCell $csv 0 39 "" ; # Baudienstleister - 1 = Bau/Reinigungs-Dienstleister 
        CkCsv_SetCell $csv 0 40 "" ; # Lief-Nr. bei Kunde - Die eigene Lieferantennummer beim Kunden 
        switch $locale {
            de_DE {
                set ausgabesprache 0
            }
            default {
                set ausgabesprache 1
            }
        }
        
        CkCsv_SetCell $csv 0 41 $ausgabesprache ; # Ausgabesprache - 0 = Deutsch , 1 = Englisch 
        CkCsv_SetCell $csv 0 42 "" ; # CC - Kopie der E-Mails an diese Adresse senden 
        CkCsv_SetCell $csv 0 43 "" ; # Telefon2
        CkCsv_SetCell $csv 0 44 "" ; # Lastschrift-Mandatsreferenz - Eindeutige Identifikation des Lastschrift-Mandats 
        CkCsv_SetCell $csv 0 45 "" ; # Datum Unterschrift  - Datum der Unterschrift des Lastschrift-Mandats 
        CkCsv_SetCell $csv 0 46 "" ; # 1 = Mahnsperre 
        CkCsv_SetCell $csv 0 47 "" ; # 1 = keine Mailings 
        CkCsv_SetCell $csv 0 48 "" ; # 1 = Privatperson 
        CkCsv_SetCell $csv 0 49 [string range $url 0 79]  ; # URL
        CkCsv_SetCell $csv 0 50 "" ; # 0 = Teil-Lieferungen nicht erlaubt, 1 = Teil-Lieferungen erlaubt. 
        CkCsv_SetCell $csv 0 51 "" ; # 0 = Teil-Rechnungen nicht erlaubt, 1 = Teil-Rechnungen erlaubt. 
   
        return [CkCsv_saveToString $csv]
    }

    ad_proc -public CMXLIF {
        -provider_id
        {-provider_contact_id ""}
        {-error_var ""}
    } {
        Generates a provider record for sending to collmex

        @param provider_id Company we want to send over to collmex
        @param provider_contact_id Company contact we want to send. Defaults to accounting_contact_id, then primary_contact_id. Null otherwise

        use field description from http://www.collmex.de/cgi-bin/cgi.exe?1005,1,help,daten_importieren_kunde
    } {
        if {$provider_contact_id eq ""} {
            set provider_contact_id [db_string accounting "select coalesce(accounting_contact_id,primary_contact_id) from im_companies where company_id = :provider_id" -default ""]
        }

        if {$error_var ne ""} {
            upvar $error_var errors
        } else {
            set errors ""
        }

        if {$provider_contact_id eq ""} {
            set errors "Could not find an accounting or primary contact for the company [im_name_from_id $provider_id]."
            intranet_collmex::error -object_id $provider_id -message $errors
            return ""
        } else {
            db_1row contact_info {
                select * from cc_users where user_id = :provider_contact_id
            }
        }

        set csv [new_CkCsv]
        CkCsv_put_Delimiter $csv ";"

        db_1row customer_info {
            select *
                from im_offices o, im_companies c
            where c.main_office_id = o.office_id
            and c.company_id = :provider_id
        }


        # Translation of the country code
        switch $address_country_code {
            "uk" {set address_country_code gb}
            ""   {set address_country_code de} ; # default country code germany
        }

        if {$address_city eq ""} {
            set errors "Provider [im_name_from_id $provider_id] does not have a city"
            intranet_collmex::error -object_id $provider_id -message $errors            
            return ""
        }
        
        CkCsv_SetCell $csv 0 0 "CMXLIF"    
             if {[exists_and_not_null collmex_id]} {
            CkCsv_SetCell $csv 0 1 $collmex_id    
        }

        set locale [lang::user::site_wide_locale -user_id $provider_contact_id]
        CkCsv_SetCell $csv 0 2 1 ; # Firma Nr (internal)
        CkCsv_SetCell $csv 0 3 [string range [im_category_from_id -current_user_id $provider_contact_id -locale $locale $salutation_id] 0 9] ; # Anrede
        CkCsv_SetCell $csv 0 4 [string range $position 0 9] ; # Title
        CkCsv_SetCell $csv 0 5 [string range $first_names 0 39] ; # Vorname
        CkCsv_SetCell $csv 0 6 [string range $last_name 0 39];# Name
        if {[string match "Freelance*" $company_name]} {
            CkCsv_SetCell $csv 0 7 "" ; # Firma - not present, single freelancer without company name
        } else {
            CkCsv_SetCell $csv 0 7 [string range $company_name 0 79] ; # Firma
        }
        CkCsv_SetCell $csv 0 8 [string range $job_title 0 39] ; # Abteilung
        
        if {$address_line2 ne ""} {
            append address_line1 "\n $address_line2"
        }        

        CkCsv_SetCell $csv 0 9 [string range $address_line1 0 39] ; # Straße
        
        CkCsv_SetCell $csv 0 10 [string range $address_postal_code 0 9] ; # PLZ
        CkCsv_SetCell $csv 0 11 [string range $address_city 0 39] ; # Ort
        CkCsv_SetCell $csv 0 12 [string range $note 0 1023] ; # Bemerkung

        switch $company_status_id {
            49 {
                # Deleted
                set company_status 3
            }
            47 - 48 - 50 {
                # Inactive
                set company_status 1
            }
            default {
                set company_status 0
            }
        }
        CkCsv_SetCell $csv 0 13 $company_status ; # Inaktiv
        CkCsv_SetCell $csv 0 14 [string range $address_country_code 0 1] ; # Land
        CkCsv_SetCell $csv 0 15 [string range $phone 0 39] ; # Telefon
        CkCsv_SetCell $csv 0 16 [string range $fax 0 39] ; # Telefax
        CkCsv_SetCell $csv 0 17 [string range $email 0 49] ; # E-Mail
        CkCsv_SetCell $csv 0 18 [string range $bank_account_nr 0 19] ; # Kontonr
        CkCsv_SetCell $csv 0 19 [string range $bank_routing_nr 0 19] ; # Blz
        CkCsv_SetCell $csv 0 20 [string range $iban 0 39] ; # Iban
        CkCsv_SetCell $csv 0 21 [string range $bic 0 19] ; # Bic
        CkCsv_SetCell $csv 0 22 [string range $bank_name 0 39] ; # Bankname
        CkCsv_SetCell $csv 0 23 [string range $tax_number 0 19] ; # Steuernummer
        CkCsv_SetCell $csv 0 24 [string range $vat_number 0 19] ; # USt.IdNr
        CkCsv_SetCell $csv 0 25 "6" ; # Zahlungsbedingung - als Zahl codiert, beginnend mit 0 für 30T ohne Abzug, wie im Programm unter "Einstellungen" 
        CkCsv_SetCell $csv 0 26 "" ; # Lieferbedingung - International genormte INCOTERMS 
        CkCsv_SetCell $csv 0 27 "" ; # Lieferbedingung Zusatz - Ort der Lieferbedingung 
        CkCsv_SetCell $csv 0 28 "1" ; # Ausgabemedium - Als Zahl codiertes Ausgabemedium: 0 = Druck, 1 = E-Mail, 2 = Fax, 3 = Brief. Standard ist E-Mail. 
        CkCsv_SetCell $csv 0 29 "" ; # Kontoinhaber - nur erforderlich falls abweichend 
        CkCsv_SetCell $csv 0 30 "" ; # Adressgruppe - interne Nummer der Adressgruppe, wie sie bei der Pflege der Adressgruppen in der ersten Spalte angezeigt wird. Soll der Kunde mehreren Adressgruppen zugeordnet werden, sind diese jeweils durch ein Komma zu trennen (z.B. 1,2,3). Der Wert '0' entfernt den Kunden aus allen Adressgruppen. Wird nichts angegeben, bleibt die Zuordnung des Kunden zu den Gruppen unverändert. 
        CkCsv_SetCell $csv 0 31 "" ; # Kundennummer beim Lieferanten
        CkCsv_SetCell $csv 0 32 "EUR" ; # Währung (ISO-Codes)
        CkCsv_SetCell $csv 0 33 "" ; # Telefon2
        switch $locale {
            de_DE {
                set ausgabesprache 0
            }
            default {
                set ausgabesprache 1
            }
        }
        
        CkCsv_SetCell $csv 0 34 $ausgabesprache ; # Ausgabesprache - 0 = Deutsch , 1 = Englisch 
        CkCsv_SetCell $csv 0 35 "" ; # Aufwandskonto
        CkCsv_SetCell $csv 0 36 "" ; # 0 = voller USt, 1 = ermäßigte USt, 2 = keine USt. 
        CkCsv_SetCell $csv 0 37 "" ; # Buchungstext
        CkCsv_SetCell $csv 0 38 "" ; # Kostenstelle

        return [CkCsv_saveToString $csv]
    }

    ad_proc -public CMXLRN {
        -invoice_id:required
        -storno:boolean
    } {
        Import a Lieferantenrechnung into Collmex

        @param invoice_id Invoice to Import
        @return CSV String
    } {
        # Get all the invoice information
        db_1row invoice_data {
            select collmex_id,to_char(effective_date,'YYYYMMDD') as invoice_date, invoice_nr,
            round(vat,0) as vat, round(amount,2) as netto, c.company_id, address_country_code, 
            ca.aux_int2 as konto, cc.cost_center_code as kostenstelle,
            cb.aux_int2 as collmex_payment_term_id, currency, ci.vat_type_id
            from im_invoices i, im_costs ci, im_companies c, im_offices o, im_categories ca, im_cost_centers cc, im_categories cb
            where c.company_id = ci.provider_id
            and c.main_office_id = o.office_id
            and ci.cost_id = i.invoice_id
            and ca.category_id = ci.vat_type_id
                and cb.category_id = ci.payment_term_id
                and cc.cost_center_id = ci.cost_center_id
            and i.invoice_id = :invoice_id
        }

        set csv [new_CkCsv]
        CkCsv_put_Delimiter $csv ";"

        regsub -all {\.} $netto {,} netto
        CkCsv_SetCell $csv 0 0 "CMXLRN"    

        if {$collmex_id eq ""} {
            set collmex_id [intranet_collmex::update_company -company_id $company_id]
        }

        if {$collmex_id eq ""} {
            intranet_collmex::error -object_id $invoice_id -message "Cannot create [im_name_from_id $invoice_id] as the provider [im_name_from_id $company_id] can't be created in Collmex. Fix your master data first"
            return ""
        }

        CkCsv_SetCell $csv 0 1 "$collmex_id" ; # Lieferantennummer
        CkCsv_SetCell $csv 0 2 "1" ; # Firma Nr
        CkCsv_SetCell $csv 0 3 "$invoice_date" ; # Rechnungsdatum
        CkCsv_SetCell $csv 0 4 "$invoice_nr" ; # Rechnungsnummer

        if {$konto eq ""} {
            #set konto [parameter::get_from_package_key -package_key "intranet-collmex" -parameter "KontoBill"]
        }

        # Find if the provide is from germany and has vat.
        if {$vat eq 19 || $vat eq 16} {
            CkCsv_SetCell $csv 0 5 $netto ; # Nettobetrag voller Umsatzsteuersatz
        } else {
            CkCsv_SetCell $csv 0 5 ""
        }

        CkCsv_SetCell $csv 0 6 "" ; # Steuer zum vollen Umsatzsteuersatz
        CkCsv_SetCell $csv 0 7 "" ; # Nettobetrag halber Umsatzsteuersatz
        CkCsv_SetCell $csv 0 8 "" ; # Steuer zum halben Umsatzsteuersatz
        if {$vat eq 19 || $vat eq 16} {
            CkCsv_SetCell $csv 0 9 ""
            CkCsv_SetCell $csv 0 10 ""
        } else {
            CkCsv_SetCell $csv 0 9 $konto ; # Sonstige Umsätze: Konto Nr.
            CkCsv_SetCell $csv 0 10 $netto ; # Sonstige Umsätze: Betrag
        }
        if {$currency eq ""} {set currency "EUR"}
        
        CkCsv_SetCell $csv 0 11 $currency; # Währung (ISO-Codes)
        CkCsv_SetCell $csv 0 12 "" ; # Gegenkonto (1600 per default)
        CkCsv_SetCell $csv 0 13 "" ; # Gutschrift
        CkCsv_SetCell $csv 0 14 "" ; # Belegtext
        CkCsv_SetCell $csv 0 15 "$collmex_payment_term_id" ; # Zahlungsbedingung - Optional. Als Zahl codiert, beginnend mit 0 für 30T ohne Abzug, wie im Programm unter "Einstellungen". Wenn nicht angegeben, wird die Zahlungsbedingung aus dem Kundenstamm ermittelt. 
        if {$vat eq 19 || $vat eq 16} {
            CkCsv_SetCell $csv 0 16 "$konto" ; # KontoNr voller Umsatzsteuersatz - Optional. 3200 (Wareneingang) falls nicht angegeben. 
        } else {
            CkCsv_SetCell $csv 0 16 ""
        }
        CkCsv_SetCell $csv 0 17 "" ; # KontoNr halber Umsatzsteuersatz - Optional. 3200 (Wareneingang) falls nicht angegeben. 
        if {$storno_p} {
            CkCsv_SetCell $csv 0 18 "1" ; # Storno - Umsatz = leer; stornierter Umsatz = 1
        } else {
            CkCsv_SetCell $csv 0 18 "" ; # Storno
        }
        CkCsv_SetCell $csv 0 19 "$kostenstelle" ; # Kostenstelle
        return [CkCsv_saveToString $csv]
    }

    ad_proc -public ACCDOC_GET {
        {-invoice_id ""}
        {-booking_year ""}
        {-booking_id ""}
        {-company_id ""}
        {-all_p 1}
        {-start_date ""}
        {-end_date ""}
        {-account_id ""}
        {-storno_p 0}
    } {
        Looks up the accounting information for an invoice

        @param invoice_id Invoice for which we want to get accounting information
        @param booking_year Year of the booking we are looking for
        @param booking_id ID of the booking we are looking for in collmex
        @param company_id Company for which we want to retrieve the bookings
        @param start_date Belegdatum von YYYYMMDD
        @param end_date Belegdatum bis YYYYMMDD
        @param account_id Kontonummer
        @param storno_p 0 oder leer = Keine stornierten oder Stornobuchungen, 1 = auch stornierte und Stornobuchungen 
        use field description from https://www.collmex.de/c.cmx?1005,1,help,api_Buchungen
    } {
        if {$all_p} {
            set changed_p 0
        } else {
            set changed_p 1
        }

        if {$invoice_id ne ""} {
            set invoice_nr [db_string invoice "select invoice_nr from im_invoices where invoice_id = :invoice_id" -default ""]
        } else {
            set invoice_nr ""
        }

        set customer_collmex_id ""
        set provider_collmex_id ""

        if {$company_id ne ""} {
            if {![db_0or1row company "select company_type_id, collmex_id from im_companies where company_id = :company_id"]} {
                cog_log Error "OpenItemsGet - can't find company $company_id"
                return ""
            }
            if {$collmex_id eq ""} {
                return ""
            }
            if {[lsearch [im_sub_categories [im_company_type_provider]] $company_type_id]<0} {
                set provider_p 0
                set customer_collmex_id $collmex_id
            } else {
                set provider_p 1
                set provider_collmex_id $collmex_id                
            }
        }

        set csv [new_CkCsv]
        CkCsv_put_Delimiter $csv ";"
        CkCsv_SetCell $csv 0 0 "ACCDOC_GET"
        CkCsv_SetCell $csv 0 1 "1" ; # Firma Nr
        CkCsv_SetCell $csv 0 2 "$booking_year" ; # Geschäftsjahr
        CkCsv_SetCell $csv 0 3 "$booking_id" ; # Buchungsnummer
        CkCsv_SetCell $csv 0 4 "$account_id" ; # Kontonummer
        CkCsv_SetCell $csv 0 5 "" ; # Kostenstelle
        CkCsv_SetCell $csv 0 6 "$customer_collmex_id" ; # Kundennummer (collmex)
        CkCsv_SetCell $csv 0 7 "$provider_collmex_id" ; # Lieferantennummer
        CkCsv_SetCell $csv 0 8 "" ; # Anlagenummer
        CkCsv_SetCell $csv 0 9 "$invoice_nr" ; # Rechnungsnummer
        CkCsv_SetCell $csv 0 10 ""; # Reisenummer
        CkCsv_SetCell $csv 0 11 ""; # Text (suche in buchungstext und memo)
        CkCsv_SetCell $csv 0 12 "$start_date"; # Belegdatum von
        CkCsv_SetCell $csv 0 13 "$end_date"; # Belegdatum bis
        CkCsv_SetCell $csv 0 14 "$storno_p"; # Stornos 0/leer kein, 1 mit stornos
        CkCsv_SetCell $csv 0 15 "$changed_p"; # Nur geänderte
        CkCsv_SetCell $csv 0 16 "projop"; # Systemname
        CkCsv_SetCell $csv 0 17 ""; # Zahlungsnummer
        return [CkCsv_saveToString $csv]
    } 

    ad_proc -public OPEN_ITEMS_GET {
        { -provider_p 0}
        { -company_id ""}
    } {
        Prepares the query for open items
        
        @param provider Do we query for providers (default is customers)
        @param company_id Company for which we want to query the open items. This will overwrite the provider setting if you query a customer
    } {
        set customer_collmex_id ""
        set provider_collmex_id ""

        if {$company_id ne ""} {
            if {![db_0or1row company "select company_type_id, collmex_id from im_companies where company_id = :company_id"]} {
                cog_log Error "OpenItemsGet - can't find company $company_id"
                return ""
            }
            if {[lsearch [im_sub_categories [im_company_type_provider]] $company_type_id]<0} {
                set provider_p 0
                set customer_collmex_id $collmex_id
            } else {
                set provider_p 1
                set provider_collmex_id $collmex_id                
            }
        }
        set csv [new_CkCsv]
        CkCsv_put_Delimiter $csv ";"
        CkCsv_SetCell $csv 0 0 "OPEN_ITEMS_GET"
        CkCsv_SetCell $csv 0 1 "1" ; # Firma Nr
        CkCsv_SetCell $csv 0 2 "$provider_p" ; # 0 oder leer = Kunde, 1 = Lieferant
        CkCsv_SetCell $csv 0 3 "$customer_collmex_id" ; # Kunden Nr
        CkCsv_SetCell $csv 0 4 "$provider_collmex_id" ; # Lieferanten NR
        CkCsv_SetCell $csv 0 5 "" ; # Vermittler

        return [CkCsv_saveToString $csv]
    }

    ad_proc -public INVOICE_PAYMENT_GET {
        { -invoice_id ""}
        { -new_p "0"}
    } {
        Die Abfrage von Zahlungseingängen zu importierten Rechnungen erfolgt über die Satzart INVOICE_PAYMENT_GET. Damit stehen im externen System Informationen über den in Collmex gebuchten Zahlungseingang zur Verfügung.
        Achtung: Die Funktion funktioniert nur für über die Satzart CMXUMS importiere Umsätze, nicht für Rechnungen, die in Collmex selbst erzeugt wurden.         
        
        @param invoice_id Optional. Falls nicht angegeben, werden die Zahlungen zu allen Rechnungen zurückgegeben. 
        @param new_p 1 = Es werden nur Zahlungen zu Rechnungen zurückgegeben, für die seit der letzten Abfrage Zahlungseingänge neu gebucht oder storniert wurden. 
    } {
        if {$invoice_id ne ""} {
            set invoice_nr [db_string invoice_nr "select invoice_nr from im_invoices where invoice_id = :invoice_id" -default ""]
        } else {
            set invoice_nr ""
        }
        
        set csv [new_CkCsv]
        CkCsv_put_Delimiter $csv ";"
        CkCsv_SetCell $csv 0 0 "INVOICE_PAYMENT_GET"
        CkCsv_SetCell $csv 0 1 "1" ; # Firma Nr
        CkCsv_SetCell $csv 0 2 "$invoice_nr" ; # Rechnungsnummer
        CkCsv_SetCell $csv 0 3 "$new_p" ; # Nur neue Zahlungen
        CkCsv_SetCell $csv 0 4 "" ; # Name des externen Systems. Wenn angegeben, werden nur Zahlungen für Rechnungen zurückgegeben, die von diesem System eingebucht wurden.

        return [CkCsv_saveToString $csv]
    }

    ad_proc -public ACCBAL_GET {
        { -year ""}
        { -month ""}
        { -account ""}
        { -account_group ""}
        { -company_id ""}
        { -collmex_cost_center ""}
    } {
        Die Abfrage von Salden aus der Buchhaltung erfolgt über die Satzart ACCBAL_GET. 

        @param year Geschäftsjahr - defaults to current year
        @param month Monat bis zu dem der Saldo ermittelt wird
        @param account Kontonummer
        @param account_group Kontengruppe 1 = Sonstiges, 2 = Anlagevermögen, 3 = Finanzkonten, 4 = Einnahmen, 5 = Ausgaben, 6 = Umsatzsteuer, 7 = Vorsteuer, 8 = Forderungen, 9 = Verbindlichkeiten, 10 = Abschreibungen, 11 = Rückstellungen, 12 = Bestand, 13 = Privat, 14 = Nicht abziehbare Ausgaben 
        @param company_collmex_id Kunden / Lieferanten Nr. Wenn angegeben, wird 'month' nicht berücksichtigt. 
        @param collmex_cost_center Kostenstelle
    } {
        if {$year eq ""} {
            set year [clock format [clock seconds] -format "%Y"]
        }
        if {$month ne ""} {
            set date [db_string date "SELECT (date_trunc('month', '${year}-${month}-01'::date) + interval '1 month' - interval '1 day')::date" -default ""]
        } else {
            set date ""
        }

        set customer_collmex_id ""
        set provider_collmex_id ""

        if {$company_id ne ""} {
            if {![db_0or1row company "select company_type_id, collmex_id from im_companies where company_id = :company_id"]} {
                cog_log Error "OpenItemsGet - can't find company $company_id"
                return ""
            }
            if {[lsearch [im_sub_categories [im_company_type_provider]] $company_type_id]<0} {
                set provider_p 0
                set customer_collmex_id $collmex_id
            } else {
                set provider_p 1
                set provider_collmex_id $collmex_id                
            }
        }

        set csv [new_CkCsv]
        CkCsv_put_Delimiter $csv ";"
        CkCsv_SetCell $csv 0 0 "ACCBAL_GET"
        CkCsv_SetCell $csv 0 1 "1"
        CkCsv_SetCell $csv 0 2 "$year"
        CkCsv_SetCell $csv 0 3 "$date"
        CkCsv_SetCell $csv 0 4 "$account"
        CkCsv_SetCell $csv 0 5 "$account_group"
        CkCsv_SetCell $csv 0 6 "$customer_collmex_id"
        CkCsv_SetCell $csv 0 7 "$provider_collmex_id"
        CkCsv_SetCell $csv 0 8 "$collmex_cost_center"

        return [CkCsv_saveToString $csv]
    }
    
    ad_proc -public ACCDOC {
        -csv:required
        -row:required
        -line_array:required
    } {
        Sets an array with the information in the row for an ACCDOC
        see http://collmex3.de/c.cmx?1005,1,help,api_Buchungen for return values

        @param csv Chilkat CSV ID for the loaded accdoc 
        @param row Row we want to get entries for
        @param line_array name of the array which we will fill

        @return booking_year integer Geschäftsjahr
        @return booking_id integer Buchungsnummer
        @return booking_date string Belegdatum Format YYYYMMDD
        @return creation_date string Gebucht am Format YYYYMMDD
        @return booking_text string Buchungstext
        @return booking_pos integer Positionsnummer
        @return account_id integer Kontonummer
        @return account_name string Kontoname
        @return soll_p boolean Soll/Haben
        @return booking_amount numeric Betrag
        @return customer_collmex_id
        @return customer_name
        @return provider_collmex_id
        @return provider_name
        @return asset_id
        @return asset_name
        @return cancellation_booking_id Bei Stornobuchungen die Nummer der stornierten Buchung 
        @return cost_center
        @return invoice_nr
        @return customer_order_nr Bei Anzahlungen die Nummer des zugeordneten Kundenauftrags 
        @return travel_cost_nr 
        @return cost_center
        @return op_id Beim Ausgleich von offenen Posten die Zuordnung zur ausgeglichenen Buchungsposition (see op_year op_pos)
        @return op_year
        @return op_pos
        @return linked_booking_id Die Nummer des verknüpften Belegs aus der Belegverwaltung.
        @return internal_note
        @return booking_user Benutzer, der die Buchung ausgeführt hat. 

    } {
        upvar 1 $line_array array_val
        unset -nocomplain array_val
        set array_val(booking_year) [CkCsv_getCell $csv $row 2]
        set array_val(booking_id) [CkCsv_getCell $csv $row 3]
        set array_val(booking_text) [CkCsv_getCell $csv $row 6]
        set array_val(booking_pos) [CkCsv_getCell $csv $row 7] 
        set array_val(account_id) [CkCsv_getCell $csv $row 8]
        set array_val(account_name) [CkCsv_getCell $csv $row 9]
        if {[CkCsv_getCell $csv $row 10] eq "Soll" || [CkCsv_getCell $csv $row 10] eq "0"} {
            set array_val(soll_p) 1
        } else {
            set array_val(soll_p) 0
        }
        regsub {,} [CkCsv_getCell $csv $row 11] {.} array_val(booking_amount)
        set array_val(customer_collmex_id) [CkCsv_getCell $csv $row 12]
        set array_val(customer_name) [CkCsv_getCell $csv $row 13]
        set array_val(provider_collmex_id) [CkCsv_getCell $csv $row 14]
        set array_val(provider_name) [CkCsv_getCell $csv $row 15]
        set array_val(asset_id) [CkCsv_getCell $csv $row 16]
        set array_val(asset_name) [CkCsv_getCell $csv $row 17]
        set array_val(cancellation_booking_id) [CkCsv_getCell $csv $row 18]
        set array_val(cost_center) [CkCsv_getCell $csv $row 19]
        set array_val(invoice_nr) [CkCsv_getCell $csv $row 20]
        set array_val(customer_order_nr) [CkCsv_getCell $csv $row 21]        
        set array_val(travel_cost_nr) [CkCsv_getCell $csv $row 22]
        set array_val(op_id) [CkCsv_getCell $csv $row 23]
        set array_val(op_year) [CkCsv_getCell $csv $row 24]
        set array_val(op_pos) [CkCsv_getCell $csv $row 25]
        set array_val(linked_booking_id) [CkCsv_getCell $csv $row 26]
        set array_val(booking_date) [CkCsv_getCell $csv $row 27]
        set array_val(creation_date) [CkCsv_getCell $csv $row 28]
        set array_val(internal_note) [CkCsv_getCell $csv $row 29]
        set array_val(booking_user) [CkCsv_getCell $csv $row 30]

    }
}
