# packages/intranet-collmex/tcl/intranet-collmex-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#

ad_library {
    
    Procedure to interact with collmex
    
    @author <yourname> (<your email>)
    @creation-date 2012-01-04
    @cvs-id $Id$
}

namespace eval intranet_collmex {
    ad_proc -public error {
        {-object_id ""}
        -message:required
    } {
        Throws an error which will notify the accounting_contact_id

        @param object_id Object which resulted in the error
        @param message Message with the error 
    } {
        set accounting_contact_id [db_string accounting "select accounting_contact_id from im_companies where company_path = 'internal'" -default [ad_host_administrator]]	

        set subject "Collmex Error" 
        if {$object_id ne ""} {
            lappend subject " for [im_name_from_id $object_id]"
        }
        set project_id [db_string project "select project_id from im_costs where cost_id = :object_id" -default ""]
        if {[intranet_chilkat::send_mail \
            -to_party_id $accounting_contact_id \
            -from_party_id $accounting_contact_id \
            -subject "$subject" \
            -body "There was a error in collmex, data is not transferred.<p /><br />$message" \
            -no_callback] eq ""} {
            cog_log Error "We could not send the mail due to mail server complications. Contact your sysadmin with the project_nr, freelance and current date/time"
        }
        if {[info commands "webix::notification::create"] ne ""} {
            set recipient_id [auth::get_user_id]
            webix::notifications::create -object_id $object_id -recipient_id $recipient_id -message "$subject: $message" -project_id $project_id
            webix::notifications::create -object_id $object_id -recipient_id $accounting_contact_id -message "$subject: $message" -project_id $project_id
        }

        callback intranet_collmex::error -object_id $object_id -message $message

        cog_log -object_id $object_id Error $message
    }
}

ad_proc -public intranet_collmex::http_post {
    {-csv_data ""}
    {-object_id ""}
    {-error_var ""}
} {
    Post to collmex
} {
    if {$csv_data eq ""} {
        return "-1"
    }

    # Make sure we can use HTTPS
    ::http::register https 443 ::tls::socket

    set customer_nr [parameter::get_from_package_key -package_key intranet-collmex -parameter CollmexKundenNr]
    set login [parameter::get_from_package_key -package_key intranet-collmex -parameter Login]
    set password  [parameter::get_from_package_key -package_key intranet-collmex -parameter Password]
    set active_p [parameter::get_from_package_key -package_key intranet-collmex -parameter ActiveP]

    set data "LOGIN;$login;$password\n${csv_data}\n"

    set meldungstyp ""

    if {$active_p} {
            
        # Save the content to a file.
        set tmp_path [ns_mktemp]
        set file [open $tmp_path w]
        fconfigure $file -encoding "iso8859-1"
        puts $file $data
        flush $file
        close $file

        set http [new_CkHttp]
        set reqBody [new_CkByteData]
        set fac [new_CkFileAccess]
        set success [CkFileAccess_ReadEntireFile $fac $tmp_path $reqBody]
        set reply [encoding convertfrom iso8859-1 [CkHttp_postBinary $http "https://www.collmex.de/cgi-bin/cgi.exe?$customer_nr,0,data_exchange" $reqBody "text/csv" 0 0]]
        set status_code [CkHttp_get_LastStatus $http]
        file delete $tmp_path
       
        if {$status_code ne 200} {
            set meldungstyp "E"
            set response "$status_code ... $reply"
        } else {
            set response $reply
            set meldungstyp [lindex [split $response ";"] 1]
        }
        
        set fo [open "[acs_root_dir]/log/collmex.access.log" a]
        set now [clock format [clock seconds] -format "%Y-%m-%d %H:%M:%S"]
        puts $fo "$now [im_name_from_id [auth::get_user_id]] $csv_data \n$response"
        close $fo  	

        switch $meldungstyp {
            S {
                return $response
            }
            W {
                # Warning Mail
                intranet_collmex::error -object_id $object_id -message "There was a warning in collmex for [im_name_from_id $object_id], but the call was successful. <p /> \
                        <br />Called: $csv_data \
                        <br />Reponse $response"
                return $response
            }
            E {
                if {[string match "INVOICE_PAYMENT_GET*" $csv_data]} {
                    # It is not a critical issue if we can't get the payments, therefore we only
                    # log the issue as a notice
                    cog_log Notice "Error in Collmex: $response when calling $csv_data"
                } else {
                    # Error Mail
                    intranet_collmex::error -object_id $object_id -message "There was a error in collmex for [im_name_from_id $object_id], and the call was not successful. <p /> \
                        <br />Called: $csv_data \
                        <br />Reponse $response"
                }
                if {$error_var ne ""} {
                    upvar $error_var error
                    set error $response
                }
                return "-1"
            }
            default {
                return $response
            }
        }
    } else {
        intranet_collmex::error -object_id $object_id -message "Collmex not active"
        return "-1"
    }
}

ad_proc -public intranet_collmex::update_company {
    -company_id
    -customer:boolean
    -create:boolean
    {-error_var ""}
} {
    send the company to collmex for update

    use field description from http://www.collmex.de/cgi-bin/cgi.exe?1005,1,help,daten_importieren_kunde
} {
    if {$error_var ne ""} {
        upvar 1 $error_var errors
    } else {
        set errors ""
    }

    if {![db_0or1row company_type "select company_type_id, collmex_id from im_companies where company_id = :company_id"]} {
        set errors "Collmex company import $company_id is not a company"
        cog_log Error $errors
        return ""
    }

    set csv_line ""

    if {[lsearch [im_sub_categories [im_company_type_customer]] $company_type_id] >-1} {
        set csv_line [intranet_collmex::CMXKND -customer_id $company_id -error_var "errors"]
    }

    if {[lsearch [im_sub_categories [im_company_type_provider]] $company_type_id] >-1} {
        set csv_line [intranet_collmex::CMXLIF -provider_id $company_id -error_var "errors"]
    }

    if {$csv_line eq ""} {
        if {!$create_p} {
            # ignore this mail when we create the company using a callback.
            intranet_collmex::error -message $errors
        } 
        return ""
    }

    set response [intranet_collmex::http_post -csv_data $csv_line -object_id $company_id -error_var "errors"]
    
    if {$response != "-1"} {
        set response [split $response ";"]
        if {[lindex $response 0] == "NEW_OBJECT_ID"} {
            
            set collmex_id [lindex $response 1]
            # This seems to be a new customer
            db_dml update_collmex_id "update im_companies set collmex_id = $collmex_id where company_id = :company_id"
        } 
    } else {
        set collmex_id ""
    }
    return "$collmex_id"
}

ad_proc -public intranet_collmex::update_provider_bill {
    -invoice_id
    -storno:boolean
} {
    send the provider bill to collmex
} {
    if {$storno_p} {
        set csv_line [intranet_collmex::CMXLRN -invoice_id $invoice_id -storno]
    } else {
        set csv_line [intranet_collmex::CMXLRN -invoice_id $invoice_id]
    }

    set response [intranet_collmex::http_post -csv_data $csv_line -object_id $invoice_id]
    if {$response != "-1"} {
        return [intranet_collmex::invoice_info_get -invoice_id $invoice_id]
    }
}

ad_proc -public intranet_collmex::update_customer_invoice {
    -invoice_id
    -storno:boolean
    -line_items:boolean
} {
    send the customer invoice to collmex
    
    @param invoice_id Invoice to be sent over to Collmex
    @param storno Add this flag if you want to storno the invoice
    @param line_items Add this flag if you want to transfer the individual lineitems. This only works with correctly maintained materials in the line items which link back to material groups that have a tax_id
} {
    set corr_invoice_nr ""
    
    # Get all the invoice information
    if {![db_0or1row invoice_data {
        select collmex_id,to_char(effective_date,'YYYYMMDD') as invoice_date, invoice_nr, cost_type_id,
          round(vat,0) as vat, round(amount,2) as invoice_netto, c.company_id, address_country_code, ca.aux_int1 as customer_vat,
	  date_part('year',ci.delivery_date) - date_part('year',effective_date) as year_difference,
          ca.aux_int2 as customer_konto, cc.cost_center_code as kostenstelle, cb.aux_int2 as collmex_payment_term_id, amount
        from im_invoices i, 
	im_companies c, 
	im_offices o, 
	im_categories ca, 
	im_costs ci left outer join im_cost_centers cc on (cc.cost_center_id = ci.cost_center_id) left outer join im_categories cb on (cb.category_id = ci.payment_term_id)
	where c.company_id = ci.customer_id
	    and i.invoice_id = ci.cost_id 
	    and c.main_office_id = o.office_id
            and ca.category_id = c.vat_type_id
            and i.invoice_id = :invoice_id
    }]} {
        intranet_collmex::error -object_id $invoice_id -message "Cannot create [im_name_from_id $invoice_id] in Collmex. as the data available is not sufficient."
        return ""        
    }

    if {$collmex_id eq ""} {
        set collmex_id [intranet_collmex::update_company -company_id $company_id -customer]
    }

    if {$collmex_id eq ""} {
        intranet_collmex::error -object_id $invoice_id -message "Cannot create [im_name_from_id $invoice_id] as the customer [im_name_from_id $company_id] can't be created in Collmex. Fix your master data first"
        return ""
    }
    
    # In case we did not get line_items as a boolean, check if the invoice if of vat_type for line items.
    if {$line_items_p == 0} {set line_items_p [db_string line_item_p "select 1 from im_costs where vat_type_id = 42021 and cost_id = :invoice_id" -default 0]}

    set replacement_text ""
    set verrechnungs_gutschrift ""

    switch $cost_type_id {
        3725 {
            set replacement_text "Korrigiert Rechnung"
        }
        3740 {
            set replacement_text "Storniert Rechnung"
        }
    }

    if {$cost_type_id eq 3725 || $cost_type_id eq 3740} {
	
	# This is a correction invoice. we need to link it to the original invoice in wording
	set linked_invoice_ids [relation::get_objects -object_id_two $invoice_id -rel_type "im_invoice_invoice_rel"]
	
	if {$linked_invoice_ids ne ""} {
	    db_foreach linked_list "select c.cost_id as linked_invoice_id, cost_type_id as linked_cost_type_id,cost_status_id as linked_cost_status_id, invoice_nr as linked_invoice_nr
                from im_costs c, im_invoices i 
                where cost_id in ([template::util::tcl_to_sql_list $linked_invoice_ids])
                and cost_id = invoice_id" {
		# This is a credit note, let's apply it
		if {($linked_cost_type_id == [im_cost_type_invoice] || $linked_cost_type_id == [im_cost_type_correction_invoice])} {
		    append replacement_text "$linked_invoice_nr - "
		}
	    }
	    if {$cost_type_id eq 3740} {
		# Prüfen ob wir eine Zahlung haben für die Rechnung. Nur ohne Zahlung können wir Verrechnen
		if {![intranet_collmex::invoice_payment_get -invoice_id $linked_invoice_id -all]} {
		    set verrechnungs_gutschrift "$linked_invoice_nr"
		}
	    }
	}
    }

    # ---------------------------------------------------------------
    # Deal with previous year customer konto
    # This is a manual hack at the moment
    # ---------------------------------------------------------------

    if {$year_difference >0} {
	switch $customer_konto {
	    8400 {
		set customer_konto 1718
	    }
	}
    }

    if {$line_items_p} {
        db_1row item_data {select round(sum(item_units*price_per_unit),2) as total_amount, array_to_string(array_agg(item_name), ', ') as items_text
            from (select item_units,price_per_unit,item_name from im_invoice_items ii where ii.invoice_id = :invoice_id order by sort_order) as items}
        

        set csv_line ""
        # Transfer one FI line item per invoice line
        db_foreach line_item {
            select sum(round(item_units*price_per_unit,2)) as line_item_netto, ct.aux_int1 as vat, ct.aux_int2 as line_item_konto
            from im_categories cm, im_categories ct, im_invoice_items ii, im_materials im
            where cm.aux_int2 = ct.category_id
            and ii.item_material_id = im.material_id
            and im.material_type_id = cm.category_id
            and ii.invoice_id = :invoice_id
            group by ct.aux_int1,ct.aux_int2
        } {
            # Override line item vat if the customer is tax free.
            if {$customer_vat eq 0} {
                set vat 0
                set line_item_konto $customer_konto
            }
            
            regsub -all {\.} $line_item_netto {,} netto
            regsub -all {\-} $netto {} netto

            # Create one FI line item per sales order line item
            if {$csv_line ne ""} {append csv_line "\n"}
            append csv_line "CMXUMS"; # 1
            append csv_line ";$collmex_id" ; # 2 Lieferantennummer
            append csv_line ";1" ; # 3 Firma Nr
            append csv_line ";$invoice_date" ; # 4 Rechnungsdatum
            append csv_line ";$invoice_nr" ; # 5 Rechnungsnummer
            
            if {$vat eq 19 || $vat eq 16} {
                append csv_line ";\"[im_csv_duplicate_double_quotes $netto]\"" ; # 6 Nettobetrag voller Umsatzsteuersatz
            } else {
            	append csv_line ";"
            }
            append csv_line ";" ; # 7 Steuer zum vollen Umsatzsteuersatz
            if {$vat eq 7} {
                append csv_line ";\"[im_csv_duplicate_double_quotes $netto]\"" ;  # 8 Nettobetrag halber Umsatzsteuersatz
            } else {
            	append csv_line ";"
            }
            append csv_line ";" ; # 9 Steuer zum halben Umsatzsteuersatz
            append csv_line ";" ; # 10 Umsätze Innergemeinschaftliche Lieferung
            append csv_line ";" ; # 11 Umsätze Export
            if {$vat eq 0} {
                append csv_line ";$line_item_konto" ; # 12 Steuerfreie Erloese Konto
                append csv_line ";\"[im_csv_duplicate_double_quotes $netto]\""; # Steuerfrei Betrag
            } else {
                append csv_line ";" ; # 12 Hat VAT => Nicht Steuerfrei
                append csv_line ";" ; # 13 Hat VAT => Nicht Steuerfrei
            }
            append csv_line ";\"EUR\"" ; # 14Währung (ISO-Codes)
            append csv_line ";" ; # 15 Gegenkonto
            if {$line_item_netto >=0} {
                append csv_line ";0" ; # 16 Rechnungsart
            } else {
                append csv_line ";1" ; # 16 Rechnungsart Gutschrift
            }
            append csv_line ";\"[im_csv_duplicate_double_quotes "$replacement_text $items_text"]\"" ; # 17 Belegtext
            append csv_line ";$collmex_payment_term_id" ; # 18 Zahlungsbedingung
            if {$vat eq 19 || $vat eq 16} {
            	append csv_line ";$line_item_konto" ; # 19 KontoNr voller Umsatzsteuersatz
            } else {
            	append csv_line ";" ; # 19 KontoNr voller Umsatzsteuersatz
            }
            if {$vat eq 7} {
            	append csv_line ";$line_item_konto" ; # 20 KontoNr halber Umsatzsteuersatz
            } else {
            	append csv_line ";" ; # 20 KontoNr halber Umsatzsteuersatz
            }
            append csv_line ";" ; # 21 reserviert
            append csv_line ";" ; # 22 reserviert
            if {$storno_p} {
                append csv_line ";1" ; # 23 Storno
            } else {
                append csv_line ";" ; # 23 Storno
            }
            append csv_line ";" ; # 24 Schlussrechnung
            append csv_line ";" ; # 25 Erloesart
            append csv_line ";\"projop\"" ; # 26 Systemname
            append csv_line ";\"$verrechnungs_gutschrift\"" ; # 27 Verrechnen mit Rechnugnsnummer fuer gutschrift
            append csv_line ";\"$kostenstelle\"" ; # 28 Kostenstelle
        }
    
    } else {

        regsub -all {\.} $invoice_netto {,} netto
        regsub -all {\-} $netto {} netto

        set csv_line "CMXUMS"; # 1
	
        append csv_line ";$collmex_id" ; # 2 Lieferantennummer
        append csv_line ";1" ; # 3 Firma Nr
        append csv_line ";$invoice_date" ; # 4 Rechnungsdatum
        append csv_line ";$invoice_nr" ; # 5 Rechnungsnummer

        if {$customer_konto eq ""} {
            set konto [parameter::get_from_package_key -package_key "intranet-collmex" -parameter "KontoInvoice"]
        }

        # Find if the provide is from germany and has vat.
        if {$vat eq 19 || $vat eq 16} {

            append csv_line ";\"[im_csv_duplicate_double_quotes $netto]\"" ; # 6 Nettobetrag voller Umsatzsteuersatz
        } else {
            append csv_line ";"
        }

        append csv_line ";" ; # 7 Steuer zum vollen Umsatzsteuersatz
        append csv_line ";" ; # 8 Nettobetrag halber Umsatzsteuersatz
        append csv_line ";" ; # 9 Steuer zum halben Umsatzsteuersatz
        append csv_line ";" ; # 10 Umsätze Innergemeinschaftliche Lieferung
        append csv_line ";" ; # 11 Umsätze Export
        if {$vat eq 19 || $vat eq 16} {
            append csv_line ";" ; # 12 Hat VAT => Nicht Steuerfrei
            append csv_line ";" ; # 13 Hat VAT => Nicht Steuerfrei
        } else {
            append csv_line ";$customer_konto" ; # 12 Steuerfreie Erloese Konto
            append csv_line ";\"[im_csv_duplicate_double_quotes $netto]\""; # Steuerfrei Betrag
        }
        append csv_line ";\"EUR\"" ; # 14Währung (ISO-Codes)
        append csv_line ";" ; # 15 Gegenkonto
        if {$invoice_netto >=0} {
            append csv_line ";0" ; # 16 Rechnungsart
        } else {
            append csv_line ";1" ; # 16 Rechnungsart Gutschrift
        }
        append csv_line ";$replacement_text" ; # 17 Belegtext
        append csv_line ";$collmex_payment_term_id" ; # 18 Zahlungsbedingung
        if {$vat eq 19 || $vat eq 16} {
            append csv_line ";$customer_konto" ; # 19 KontoNr voller Umsatzsteuersatz
        } else {
            append csv_line ";"
        }
        append csv_line ";" ; # 20 KontoNr halber Umsatzsteuersatz
        append csv_line ";" ; # 21 reserviert
        append csv_line ";" ; # 22 reserviert
        if {$storno_p} {
            append csv_line ";1" ; # 23 Storno
        } else {
            append csv_line ";" ; # 23 Storno
        }
        append csv_line ";" ; # 24 Schlussrechnung
        append csv_line ";" ; # 25 Erloesart
        append csv_line ";\"projop\"" ; # 26 Systemname
	append csv_line ";\"$verrechnungs_gutschrift\"" ; # 27 Verrechnen mit Rechnugnsnummer fuer gutschrift
        append csv_line ";\"$kostenstelle\"" ; # 28 Kostenstelle
    }

    set response [intranet_collmex::http_post -csv_data $csv_line -object_id $invoice_id]
    if {$response != "-1"} {
        return [intranet_collmex::invoice_info_get -invoice_id $invoice_id]
    }
}
    
ad_proc -public intranet_collmex::invoice_payment_get {
    {-invoice_id ""}
    -all:boolean
} {
    get a list of invoice payments from collmex
} {
    
    if {$all_p} {
        set new_p 0
    } else {
        set new_p 1
    }

    set payment_id ""

    # First try directly with invoice_payment_get    
    set csv_line [intranet_collmex::INVOICE_PAYMENT_GET -invoice_id $invoice_id -new_p $new_p]
    set response [intranet_collmex::http_post -csv_data $csv_line -object_id $invoice_id]

    if {$response != "-1"} {
        set csv [new_CkCsv]
        # Indicate that the 1st line contains column names.
        CkCsv_put_HasColumnNames $csv 0
        set success [CkCsv_LoadFromString $csv $response]

        set n [CkCsv_get_NumRows $csv]
        for {set row 0} {$row <= [expr $n - 1]} {incr row} {
            if {[CkCsv_getCell $csv $row 0] eq "INVOICE_PAYMENT"} {
                set invoice_nr [CkCsv_getCell $csv $row 1]
                set date [CkCsv_getCell $csv $row 2]
                regsub {,} [CkCsv_getCell $csv $row 3] {.} amount
                regsub {,} [CkCsv_getCell $csv $row 4] {.} amount_reduction
                set booking_year [CkCsv_getCell $csv $row 5]
                set booking_id [CkCsv_getCell $csv $row 6]
                set booking_pos [CkCsv_getCell $csv $row 7]
                set collmex_id  "${booking_year}-${booking_id}-${booking_pos}"

                # Check if we have this id already for a payment
	            set payment_id [db_string payment_id "select payment_id from im_payments where collmex_id = :collmex_id" -default ""]
                set invoice_id [db_string invoice_id "select invoice_id from im_invoices where invoice_nr = :invoice_nr" -default ""]

                if {$payment_id eq ""} {
                    # Find the invoice_id
                    if {$invoice_id ne "" && $collmex_id ne "--"} {
		                # Check if we received the payment already
		                set payment_id [db_string payment_id "select payment_id from im_payments where cost_id = :invoice_id and amount = :amount and received_date = to_date(:date,'YYYYMMDD')" -default ""]
		    
                        # Lets record the payment
                        if {$payment_id eq ""} {
                            set payment_id [cog::payment::create -cost_id $invoice_id -actual_amount $amount]
        		}
                    }
                }
                db_dml update "update im_payments set received_date = to_date(:date,'YYYYMMDD'), amount = :amount, collmex_id = :collmex_id where payment_id = :payment_id" 
                intranet_collmex::invoice_payment_info_get -payment_id $payment_id
            }
        }
    } 

    if {$payment_id eq ""} {
        set year [clock format [clock seconds] -format "%Y"]
        set customer_cost_type_ids [im_sub_categories [im_cost_type_invoice]]
        if {$invoice_id ne ""} {
            db_0or1row companies "select customer_id, provider_id, cost_type_id from im_costs where cost_id = :invoice_id"
            if {[lsearch $customer_cost_type_ids $cost_type_id] <0 } {
                set company_ids $provider_id
            } else {
                set company_ids $customer_id
            }
        } else {
            set exclude_cost_status_ids [list [im_cost_status_paid] [im_cost_status_deleted] [im_cost_status_cancelled] [im_cost_status_rejected]]
            set provider_cost_type_ids [im_sub_categories [im_cost_type_bill]]
            
            set company_ids [db_list customers "select distinct customer_id from im_costs 
                where cost_type_id in ([template::util::tcl_to_sql_list $customer_cost_type_ids]) 
                and cost_status_id not in ([template::util::tcl_to_sql_list $exclude_cost_status_ids])
                and to_char(effective_date,'YYYY') = :year
                UNION
                select distinct customer_id from im_costs 
                where cost_type_id in ([template::util::tcl_to_sql_list $provider_cost_type_ids]) 
                and cost_status_id not in ([template::util::tcl_to_sql_list $exclude_cost_status_ids])
                and to_char(effective_date,'YYYY') = :year
                "]
        }

        foreach company_id [lsort -unique $company_ids] {
            if {$company_id ne ""} {
                set collmex_ids [intranet_collmex::company_bookings_get -booking_year $year -company_id $company_id]
                if {$collmex_ids ne ""} {
                    set payment_id "found"
                }
            }
        }
    }

    if {$payment_id ne ""} {
	    return 1
    } else {
	    return 0
    }
}

ad_proc -public intranet_collmex::invoice_payment_info_get {
    -payment_id:required
} {
    get the payment booking
} {
    
    set collmex_payment_id [db_string payment "select collmex_id from im_payments where payment_id = :payment_id" -default ""]
    if {$collmex_payment_id ne ""} {
        set collmex_list [split $collmex_payment_id "-"]
        set booking_year [lindex $collmex_list 0]
        set booking_id [lindex $collmex_list 1]
        set booking_pos [lindex $collmex_list 2]
        set csv_line [intranet_collmex::ACCDOC_GET -booking_year $booking_year -booking_id $booking_id]
        set response [intranet_collmex::http_post -csv_data $csv_line -object_id $payment_id]
        
        if {$response != "-1"} {
            set csv [new_CkCsv]

            # Indicate that the 1st line contains column names.
            CkCsv_put_HasColumnNames $csv 0
            set success [CkCsv_LoadFromString $csv $response]

            set n [CkCsv_get_NumRows $csv]
            for {set row 0} {$row <= [expr $n - 1]} {incr row} {
                if {[CkCsv_getCell $csv $row 0] eq "ACCDOC"} {
                    if {[CkCsv_getCell $csv $row 7] eq $booking_pos} {
                		set note [CkCsv_getCell $csv $row 6] ; # Buchungstext
	    	            db_dml update_note "update im_payments set note=:note where payment_id = :payment_id"
                        cog_log Notice "$payment_id $note"
                    }
                }
            }
        }
    }
}

ad_proc -public intranet_collmex::booking_get {
    -booking_year:required
    -booking_id:required
} {
    Import a specific booking into collmex_accdoc

    @param booking_year Year of the booking
    @param booking_id ID of the booking
} {
    set csv_line [intranet_collmex::ACCDOC_GET -booking_year $booking_year -booking_id $booking_id]
    set response [intranet_collmex::http_post -csv_data $csv_line]
    if {$response ne "-1"} {
        set collmex_ids [intranet_collmex::import::accdoc -response $response]
    } else {
        set collmex_ids ""
    }
    return $collmex_ids
}

ad_proc -public intranet_collmex::import::accdoc {
    -response:required
} {
    Import bookings from a response into the collmex_accdoc table
    @param response CSV Response we get from an accdoc_get 
} {
    set csv [new_CkCsv]

    # Indicate that the 1st line contains column names.
    CkCsv_put_HasColumnNames $csv 0
    set success [CkCsv_LoadFromString $csv $response]
    set n [CkCsv_get_NumRows $csv]
    
    set collmex_ids [list]

    for {set row 0} {$row <= [expr $n - 1]} {incr row} {
        if {[CkCsv_getCell $csv $row 0] eq "ACCDOC"} {
            intranet_collmex::ACCDOC -csv $csv -row $row -line_array accdoc
            
            set booking_year $accdoc(booking_year)
            set booking_id $accdoc(booking_id)
            set booking_pos $accdoc(booking_pos)
            set customer_collmex_id [CkCsv_getCell $csv $row 12]
            set provider_collmex_id [CkCsv_getCell $csv $row 14]
            if {$customer_collmex_id eq ""} {
                set company_collmex_id $provider_collmex_id
            } else {
                set company_collmex_id $customer_collmex_id
            }

            set invoice_id [db_string invoice "select invoice_id from im_invoices where invoice_nr='$accdoc(invoice_nr)'" -default ""]
            if {$invoice_id eq ""} {
                set rel_booking_year [CkCsv_getCell $csv $row 24]
                set rel_booking_id $accdoc(linked_booking_id)
                set invoice_id [db_string invoice_from_accdoc "select distinct invoice_id from collmex_accdoc where booking_year = :booking_year and booking_id = :rel_booking_id limit 1" -default ""]
            }

            set entry_exists_p [db_string exists "select 1 from collmex_accdoc where invoice_id = :invoice_id and booking_year = :booking_year and booking_id = :booking_id and booking_pos = :booking_pos limit 1" -default 0]
            if {$entry_exists_p eq 0} {
                db_dml insert "insert into collmex_accdoc (invoice_id, booking_year, booking_id, booking_pos) values (:invoice_id, :booking_year, :booking_id, :booking_pos)"
            }

            db_dml update "update collmex_accdoc set booking_date = to_date('$accdoc(booking_date)','YYYYMMDD'), creation_date = to_date('$accdoc(creation_date)','YYYYMMDD'),
                booking_text = '$accdoc(booking_text)', account_id = $accdoc(account_id), cost_center = '$accdoc(cost_center)', internal_note = '$accdoc(internal_note)', 
                account_name = '$accdoc(account_name)', booking_amount = '$accdoc(booking_amount)', company_collmex_id = :company_collmex_id
                where invoice_id = :invoice_id and booking_year = :booking_year and booking_id = :booking_id and booking_pos = :booking_pos" 
            
            lappend collmex_ids "${booking_year}-${booking_id}-${booking_pos}"
        }
    }

    return $collmex_ids
}

ad_proc -public intranet_collmex::company_bookings_get {
    -company_id:required
    { -booking_year "" }
} {
    get the invoice booking
} {
    set csv_line [intranet_collmex::ACCDOC_GET -company_id $company_id -booking_year $booking_year]
    
    set company_collmex_id [db_string company "select collmex_id from im_companies where company_id = :company_id" -default ""]    
    set response [intranet_collmex::http_post -csv_data $csv_line -object_id $company_id]
    set collmex_ids [list]
    if {$response != "-1"} {
        set csv [new_CkCsv]

        # Indicate that the 1st line contains column names.
        CkCsv_put_HasColumnNames $csv 0
        set success [CkCsv_LoadFromString $csv $response]

        set n [CkCsv_get_NumRows $csv]
        for {set row 0} {$row <= [expr $n - 1]} {incr row} {
            if {[CkCsv_getCell $csv $row 0] eq "ACCDOC"} {
                set booking_year [CkCsv_getCell $csv $row 2]
                set booking_id [CkCsv_getCell $csv $row 3]
                set booking_date [CkCsv_getCell $csv $row 4]
                set creation_date [CkCsv_getCell $csv $row 5]
                set booking_text [CkCsv_getCell $csv $row 6]
                set booking_pos [CkCsv_getCell $csv $row 7] 
                set account_id [CkCsv_getCell $csv $row 8]
                set account_name [CkCsv_getCell $csv $row 9]
                if {[CkCsv_getCell $csv $row 10] eq "Soll" || [CkCsv_getCell $csv $row 10] eq "0"} {
                    set soll_p 1
                } else {
                    set soll_p 0
                }

                regsub {,} [CkCsv_getCell $csv $row 11] {.} booking_amount
                set customer_collmex_id [CkCsv_getCell $csv $row 12]
                set provider_collmex_id [CkCsv_getCell $csv $row 14]
                set cost_center [CkCsv_getCell $csv $row 19]
                set invoice_nr [CkCsv_getCell $csv $row 20]

                set invoice_id [db_string invoice "select invoice_id from im_invoices where invoice_nr=:invoice_nr" -default ""]
                if {$invoice_id eq ""} {
                    set rel_booking_year [CkCsv_getCell $csv $row 24]
                    set rel_booking_id [CkCsv_getCell $csv $row 26]
                    set invoice_id [db_string invoice_from_accdoc "select distinct invoice_id from collmex_accdoc where booking_year = :rel_booking_year and booking_id = :rel_booking_id limit 1" -default ""]
                }

                if {$invoice_id eq ""} {
                    continue
                }

                set internal_note [CkCsv_getCell $csv $row 29]
                
                set payment_p 0
                if {$customer_collmex_id eq ""} {
                    if {$company_collmex_id ne $provider_collmex_id} {
            			continue
		            }
		    
                    if {$soll_p} {
                        set payment_p 1
                    }
                } else {
                    if {$company_collmex_id ne $customer_collmex_id} {
        		    	continue
		            }

                    if {!$soll_p} {
                        set payment_p 1
                    }
                }

                # Still need to do conversion of charsets in cell, therefore internal note is excluded
                set entry_exists_p [db_string exists "select 1 from collmex_accdoc where invoice_id = :invoice_id and booking_year = :booking_year and booking_id = :booking_id and booking_pos = :booking_pos limit 1" -default 0]
                if {$entry_exists_p eq 0} {
                    db_dml insert "insert into collmex_accdoc (invoice_id, booking_year, booking_id, booking_pos) values (:invoice_id, :booking_year, :booking_id, :booking_pos)"
                }

                db_dml update "update collmex_accdoc set booking_date = to_date(:booking_date,'DD.MM.YYYY'), creation_date = to_date(:creation_date,'DD.MM.YYYY'),
                        booking_text = :booking_text, account_id = :account_id, cost_center = :cost_center, internal_note = :internal_note, account_name = :account_name,
                        booking_amount = :booking_amount, company_collmex_id = :company_collmex_id
                    where invoice_id = :invoice_id and booking_year = :booking_year and booking_id = :booking_id and booking_pos = :booking_pos" 

                if {$payment_p} {
                    set collmex_id  "${booking_year}-${booking_id}-${booking_pos}"

                    # Check if we have this id already for a payment
                    set payment_id [db_string payment_id "select payment_id from im_payments where collmex_id = :collmex_id" -default ""]
                    if {$payment_id eq ""} {
                        set payment_id [db_string payment_id "select payment_id from im_payments where cost_id = :invoice_id and amount = :booking_amount and received_date = to_date(:booking_date,'DD.MM.YYYY')" -default ""]
		    
                        # Lets record the payment
                        if {$payment_id eq ""} {
                            set payment_id [cog::payment::create -cost_id $invoice_id -actual_amount $booking_amount]
        		        }
                    }
                    db_dml update "update im_payments set received_date = to_date(:booking_date,'DD.MM.YYYY'), amount = :booking_amount, collmex_id = :collmex_id where payment_id = :payment_id" 
                    callback im_payment_after_update -payment_id $payment_id
                }
                lappend collmex_ids "${booking_year}-${booking_id}-${booking_pos}"
            }
        }
    }

    return $collmex_ids
}

ad_proc -public intranet_collmex::invoice_info_get {
    -invoice_id:required
    {-all_p 1}
} {
    get the invoice booking
} {
    set csv_line [intranet_collmex::ACCDOC_GET -invoice_id $invoice_id -all_p 1]
    set response [intranet_collmex::http_post -csv_data $csv_line]
    if {$response ne "-1"} {
        set collmex_ids [intranet_collmex::import::accdoc -response $response]
    } else {
        set collmex_ids ""
    }

    # Clean unknown companies    
    foreach collmex_id $collmex_ids {
        set collmex_list [split $collmex_id "-"]
        set booking_year [lindex $collmex_list 0]
        set booking_id [lindex $collmex_list 1]
        set company_collmex_id [db_string company "select distinct(company_collmex_id) from collmex_accdoc where booking_year = :booking_year and booking_id = :booking_id and company_collmex_id is not null" -default ""]
        if {$company_collmex_id ne ""} {
            set company_exists_p [db_string company "select 1 from im_companies where collmex_id = :company_collmex_id" -default 0] 
            if {!$company_exists_p} {
                db_dml del "delete from collmex_accdoc where booking_year = :booking_year and booking_id =:booking_id"
            }
        }
    }
    return $collmex_ids
}

ad_proc -public intranet_collmex::update_customer_invoices_by_nr {
    -invoice_nr_list
} {
    Resends a list of invoice numbers to collmex
    
    @param invoice_nr_list List of invoice_nr to resend to collmex
} {

    set invoice_ids [db_list invoice_ids "select invoice_id from im_invoices where invoice_nr in ([template::util::tcl_to_sql_list $invoice_nr_list ])"]
    
    foreach invoice_id $invoice_ids {
	    cog_log Notice "Creating invoice in Collmex:: [intranet_collmex::update_customer_invoice -invoice_id $invoice_id]"
    }
}

ad_proc -public intranet_collmex::open_items_get {
    {-company_id ""}
    -provider
} {
    get a list of open items from collmex
} {
    set csv_line [intranet_collmex::OPEN_ITEMS_GET -company_id $company_id -provider_p $provider_p]
    set response [intranet_collmex::http_post -csv_data $csv_line -object_id $company_id]
    set invoice_nrs [list]

    if {$response eq "-1"} {
        return ""
    } 
        
    set csv [new_CkCsv]

    # Indicate that the 1st line contains column names.
    CkCsv_put_HasColumnNames $csv 0
    set success [CkCsv_LoadFromString $csv $response]

    set n [CkCsv_get_NumRows $csv]
    for {set row 0} {$row <= [expr $n - 1]} {incr row} {
        if {[CkCsv_getCell $csv $row 0] eq "OPEN_ITEM"} {
            set booking_year [CkCsv_getCell $csv $row 2]
            set booking_id [CkCsv_getCell $csv $row 3]
            set booking_pos [CkCsv_getCell $csv $row 4]
            set customer_collmex_id [CkCsv_getCell $csv $row 5]
            set provider_collmex_id [CkCsv_getCell $csv $row 7]
            set invoice_nr [CkCsv_getCell $csv $row 9]
            set booking_date  [CkCsv_getCell $csv $row 10]
	        set amount [CkCsv_getCell $csv $row 17]
	        set paid_amount [CkCsv_getCell $csv $row 18]
	        set open_amount [CkCsv_getCell $csv $row 19]
            regsub -all {,} $amount {.} amount
            regsub -all {,} $paid_amount {.} paid_amount
            regsub -all {,} $open_amount {.} open_amount

            if {0} {
                if {$paid_amount eq ""} {
                    db_dml update_to_outstanding "update im_costs set cost_status_id = [im_cost_status_outstanding] where cost_nr = :invoice_nr"
                } else {
                    db_dml update_to_outstanding "update im_costs set cost_status_id = [im_cost_status_partially_paid] where cost_nr = :invoice_nr"
                }
            }
            lappend invoice_nrs $invoice_nr
        }
    }
    set invoice_ids [db_list invoices "select invoice_id from im_invoices where invoice_nr in ([template::util_tcl_to_sql_list $invoice_nrs])]"
    return $invoice_ids
}

ad_proc -public intranet_collmex::update_outstanding {
    { -year_month ""} 
} {
    Updates collmex wiht outstanding invoices and provider_bills
} {
    if {$year_month eq ""} {
        set year_month [clock format [clock seconds] -format "%Y-%m"]
    }
    
    db_foreach invoice "select cost_id, cost_name from im_costs where cost_name like 'I${year_month}%' and cost_status_id = [im_cost_status_outstanding] and cost_id not in (select invoice_id from collmex_accdoc)" {
        set collmex_info [intranet_collmex::invoice_info_get -invoice_id $cost_id]
        if {$collmex_info eq ""} {
            intranet_collmex::update_customer_invoice -invoice_id $cost_id
        }
    }

    db_foreach invoice "select cost_id, cost_name from im_costs where cost_name like 'B${year_month}%' and cost_status_id = [im_cost_status_outstanding] and cost_id not in (select invoice_id from collmex_accdoc)" {
        set collmex_info [intranet_collmex::invoice_info_get -invoice_id $cost_id]
        if {$collmex_info eq ""} {
            intranet_collmex::update_provider_bill -invoice_id $cost_id
        }
    }
}


ad_proc -public intranet-collmex::update_account_balance {
    { -year ""}
    { -month ""}
} {
    Updates the account balance tables for a specific year and month - ONLY WORKS WITH SKR04

    @param year Defaults to this year
    @param month Defaults to previous month - if not provided loop through all months of this year
} {
    if {$year eq ""} {
        set year [clock format [clock seconds] -format "%Y"]
    }

    if {$month ne ""} {
        set months_list $month
    } else {
        set months_list [db_list months "select to_char(date_trunc('month',generate_series),'MM') from generate_series('${year}-01-05', now(), '1 month')"]
    }

    foreach month $months_list {
        set csv_line [intranet_collmex::ACCBAL_GET -year $year -month "$month"]
        set response [intranet_collmex::http_post -csv_data $csv_line]
    
        if {$response eq "-1"} {
            continue
        } 
        
        set bal_date [db_string date "SELECT (date_trunc('month', '${year}-${month}-01'::date) + interval '1 month' - interval '1 day')::date" -default ""]

        set csv [new_CkCsv]
        # Indicate that the 1st line contains column names.
        CkCsv_put_HasColumnNames $csv 0
        set success [CkCsv_LoadFromString $csv $response]

        set n [CkCsv_get_NumRows $csv]
        for {set row 0} {$row <= [expr $n - 1]} {incr row} {
            if {[CkCsv_getCell $csv $row 0] eq "ACC_BAL"} {
                set account_id [CkCsv_getCell $csv $row 1]
                if {[lsearch [intranet-collmex::skr04_accounts] $account_id]<0} {
                    continue
                }
                set amount [CkCsv_getCell $csv $row 3]
                regsub -all {,} $amount {.} amount
                db_dml update_account_info "insert into collmex_accbal (account_id, bal_date, amount) 
                    VALUES (:account_id, :bal_date, :amount)
                    ON CONFLICT (account_id,bal_date) DO update set amount = :amount"         
            }
        }
    }
}

ad_proc -public intranet-collmex::update_accounts {
} {
    Updates the accounts so we have a record of used accounts
} {
    set year [clock format [clock seconds] -format "%Y"]
    set months_list [db_list months "select to_char(date_trunc('month',generate_series),'MM') from generate_series('${year}-01-05', now(), '1 month')"]

    set account_ids [list]

    foreach month $months_list {
        set csv_line [intranet_collmex::ACCBAL_GET -year $year -month "$month"]
        set response [intranet_collmex::http_post -csv_data $csv_line]
    
        if {$response eq "-1"} {
            continue
        } 
        
        set csv [new_CkCsv]
        # Indicate that the 1st line contains column names.
        CkCsv_put_HasColumnNames $csv 0
        set success [CkCsv_LoadFromString $csv $response]

        set n [CkCsv_get_NumRows $csv]
        for {set row 0} {$row <= [expr $n - 1]} {incr row} {
            if {[CkCsv_getCell $csv $row 0] eq "ACC_BAL"} {
                set account_id [CkCsv_getCell $csv $row 1]
                set account_name($account_id) [CkCsv_getCell $csv $row 2]
                lappend account_ids $account_id ; # Used for later updating the accounts table
            }
        }
    }

    foreach account_id [lsort -unique $account_ids] {
        set account [set account_name($account_id)]
        db_dml update_account_info "insert into collmex_accounts (account_id, account_name) 
            VALUES (:account_id, :account)
            ON CONFLICT (account_id) DO update set account_name = :account"
    }
}

ad_proc -public intranet-collmex::skr04_accounts {

} {
    Returns a list of accounts relevant for reconciliation with SKR04
} {
    set skr04_list [list 1404 1405 1406 1407 1408 3804 3805 3806 3808 3835 3837 4336 4338 4340 4400 5900 5923 5925]

    foreach account [db_list vat_accounts "select distinct aux_int2 from im_categories where category_type = 'Intranet VAT Type' and aux_int2 is not null"] {
        if {[lsearch $skr04_list $account]<0} {
            lappend skr04_list $account
        }
    }
    return $skr04_list
}
