
ad_page_contract {
    Transfer an invoice to collmex manually
    
    @param invoice_id
    @param return_url

} {
   
    { invoice_ids "" }
    { start_date "" }
    { end_date "" }
    return_url:notnull
}


if {$invoice_ids eq ""} {
    if {$start_date eq ""} {
        set start_year [expr [clock format [clock seconds] -format "%Y"]]
        set start_date "${start_year}-10-01"
    }
        
    if {$end_date eq ""} {
        set end_date [clock format [clock seconds] -format "%Y-%m-%d"]
    }

    set relevant_status_ids [parameter::get_from_package_key -package_key "intranet-collmex" -parameter "RelevantCostStatus"]
    set relevant_type_ids [list]
    foreach cost_type_id [im_sub_categories $cost_type_ids] {
        # Hide cancellation + correction
        if {[lsearch [list 3725 3735 3740 3741] $cost_type_id]<0} {
            if {[im_cost_type_is_invoice_or_quote_p $cost_type_id]} {
                lappend relevant_type_ids $cost_type_id
            } else {
                lappend relevant_type_ids $cost_type_id
            }
        }
    }

    set invoice_ids [db_list invoices_without_booking "select cost_id from im_costs where cost_status_id in ([template::util::tcl_to_sql_list $relevant_status_ids])
        and cost_id not in (select invoice_id from collmex_accdoc)
        and cost_type_id in ([template::util::tcl_to_sql_list $relevant_type_ids])
        and effective_date >= '$start_date'
        and effective_date <= '$end_date'"]
}

set errors [list]
foreach invoice_id $invoice_ids {
    db_1row invoice_info "select cost_status_id, cost_type_id from im_costs where cost_id = :invoice_id"
    set relevant_status_ids [parameter::get_from_package_key -package_key "intranet-collmex" -parameter "RelevantCostStatus"]
    if {[lsearch $relevant_status_ids $cost_status_id]>-1} {
        if {[lsearch [im_sub_categories 3700] $cost_type_id] >-1} {
            # Customer Invoice
            set collmex_result [intranet_collmex::update_customer_invoice -invoice_id $invoice_id]
            if {$collmex_result ne ""} {
                set msg "Updating invoice in Collmex [im_name_from_id $cost_status_id] [im_name_from_id $cost_type_id]:: $collmex_result"
                cog_log Notice $msg
            } else {
                lappend errors "There was an error updating [im_name_from_id $invoice_id] in collmex, please check your e-mail for the error message"
            }
        }

        if {[lsearch [im_sub_categories 3704] $cost_type_id] >-1} {
            # Provider Bill
            set collmex_result [intranet_collmex::update_provider_bill -invoice_id $invoice_id]
            if {$collmex_result ne ""} {
                set msg "Updating invoice in Collmex [im_name_from_id $cost_status_id] [im_name_from_id $cost_type_id]:: $collmex_result"
                cog_log Notice $msg
            } else {
                lappend errors "There was an error updating [im_name_from_id $invoice_id] in collmex, please check your e-mail for the error message"
            }
        }
    }
}

if {[llength $errors] >0} {
    ad_return_error "Error in updating" "<ul><li>[join $errors "</li><li>"]</li></ul>"
} else {
    ad_returnredirect $return_url
}
