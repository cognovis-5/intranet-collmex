ad_page_contract {
    Overview of differences in collmex

} {
   invoice_id:notnull
   return_url:notnull
} 

# Update the data
intranet_collmex::invoice_info_get -invoice_id $invoice_id
intranet_collmex::invoice_payment_get -invoice_id $invoice_id -all

db_multirow -extend [list company] collmex_accdoc collmex_accdoc {
    select * from collmex_accdoc where invoice_id = :invoice_id order by booking_year, booking_id,booking_pos
} {
    set company [db_string account "select company_name from im_companies where collmex_id = :company_collmex_id" -default "$company_collmex_id"]
}

template::list::create -name collmex_accdoc -multirow collmex_accdoc \
    -elements {
        booking_year {
            label "Jahr"
        }
        booking_id {
            label "Nr"
        }
        booking_pos {
            label "Pos"
        }
        booking_date {
            label "Belegdatum"
        }
        booking_text {
            label "Buchungstext"
        }
        booking_amount {
            label "Betrag"
        }
        account_id {
            label "KontoNr"
        }
        account_name {
            label "Konto"
        }
        company {
            label "Firma"
        }
        cost_center {
            label "Kostenstelle"
        }
    }
