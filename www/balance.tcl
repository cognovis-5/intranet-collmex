ad_page_contract {
    Overview of differences in collmex

} {
    { booking_year "" }
} 

set relevant_status_ids [list 3815 3804 3808 3810]
set return_url [util_get_current_url]

if {$booking_year eq ""} {
    set booking_year [expr [clock format [clock seconds] -format "%Y"]]
}

set start_date "${booking_year}-01-01"
set end_date "${booking_year}-12-31"

set where_clause_list [list "bal_date >= '$start_date'"]
lappend where_clause_list "bal_date <= '$end_date'"
lappend where_clause_list "c.account_id = a.account_id"

set sql "select bal_date, to_char(bal_date, 'MM') as month, a.account_id, account_name, abs(amount) as amount,
    to_char(date_trunc('month', bal_date) + interval '1 month' - interval '1 day','YYYY-MM-DD') AS end_of_month
    from collmex_accbal a, collmex_accounts c
    where [join $where_clause_list " and "]
    order by account_id, bal_date asc"

set previous_account_id ""

db_multirow -extend {po_amount monthly_amount color po_docs_url difference collmex_bookings_url collmex_differences_url} balance balance $sql {
    if {$previous_account_id ne $account_id} {
        set previous_month_amount 0
        set previous_account_id $account_id
    }

    set monthly_amount [format "%.2f" [expr $amount - $previous_month_amount]]
    set previous_month_amount $amount
    set beginning_of_month "${booking_year}-${month}-01"

    set po_where_clause_list [list "effective_date::date >= '$beginning_of_month'"]
    lappend po_where_clause_list "effective_date::date <= '$end_of_month'"

    lappend po_where_clause_list "cost_status_id in ([template::util::tcl_to_sql_list $relevant_status_ids])"

    switch $account_id {
        4336 - 4338 - 4340 - 4400 {
            set cost_type_ids [im_sub_categories [im_cost_type_invoice]]
	    set vat_type_ids [db_list vat_types "select category_id from im_categories where aux_int2=:account_id"]
            lappend po_where_clause_list "cost_type_id in ([template::util::tcl_to_sql_list $cost_type_ids])"
	    lappend po_where_clause_list "vat_type_id in ([template::util::tcl_to_sql_list $vat_type_ids])"
            set po_amount [db_string should_amount "select sum(amount) from im_costs where [join $po_where_clause_list " and "]"]
        }
        5900 - 5923 - 5925 {
            set cost_type_ids [im_sub_categories [im_cost_type_bill]]
	    set vat_type_ids [db_list vat_types "select category_id from im_categories where aux_int2=:account_id"]
            lappend po_where_clause_list "cost_type_id in ([template::util::tcl_to_sql_list $cost_type_ids])"
            lappend po_where_clause_list "vat_type_id in ([template::util::tcl_to_sql_list $vat_type_ids])"
            set po_amount [db_string should_amount "select sum(amount) from im_costs where [join $po_where_clause_list " and "]"]
        }
        default {
            set cost_type_ids ""
            set po_amount ""
        }
    }

    if {$po_amount eq ""} {
	# VAT accounts
	set vat_type_ids ""
	switch $account_id {
            1405 {
		set cost_type_ids [im_sub_categories [im_cost_type_bill]]
		set vat_type_ids 42002
	    }
            1406 {
		set cost_type_ids [im_sub_categories [im_cost_type_bill]]
		set vat_type_ids 42000
	    }
	    1407 {
		set cost_type_ids [im_sub_categories [im_cost_type_bill]]
		set vat_type_ids [list 42010 42020]
	    }
	    3805 {
		set cost_type_ids [im_sub_categories [im_cost_type_invoice]]
		set vat_type_ids 42051
	    }
	    3806 {
		set cost_type_ids [im_sub_categories [im_cost_type_invoice]]
		set vat_type_ids 42050
	    }
	    3837 {
		set cost_type_ids [im_sub_categories [im_cost_type_invoice]]
		set vat_type_ids [list 42030 42040]
	    }
	    default {
		set vat_type_ids ""
	    }
	}
	if {$vat_type_ids ne ""} {
	    lappend po_where_clause_list "cost_type_id in ([template::util::tcl_to_sql_list $cost_type_ids])"
            lappend po_where_clause_list "vat_type_id in ([template::util::tcl_to_sql_list $vat_type_ids])"
            set po_amount [db_string should_amount "select sum(vat*amount/100) from im_costs where [join $po_where_clause_list " and "]"]
	} else {
	    set po_amount ""
	}
    }
    

    set collmex_differences_url ""
    set difference ""

    if {$po_amount eq ""} {
	set po_amount 0
    }
    
        set po_amount [format "%.2f" $po_amount]
        set po_docs_url [export_vars -base "po-docs" -url {{vat_type_ids $vat_type_ids} {cost_status_ids $relevant_status_ids} {cost_type_ids $cost_type_ids} {start_date $beginning_of_month} {end_date $end_of_month}}]

        if {$po_amount ne $monthly_amount} {
            set collmex_differences_url [export_vars -base "collmex-balance" -url {{start_date $beginning_of_month} {end_date $end_of_month} account_id return_url}]
            set color "red"
            set difference [format "%.2f" [expr $po_amount - $monthly_amount]]
        } else {
            set color "green"
        }


    if {$monthly_amount >0} {
        set collmex_bookings_url [export_vars -base "collmex-balance" -url {{start_date $beginning_of_month} {end_date $end_of_month} {hide_imported_p 0} account_id return_url}]	
    } else {
        set collmex_bookings_url ""
    }
}

template::list::create -name balance -multirow balance \
    -elements {
        month {
            label "Monat"
        }
        po_amount {
            label "Collmex SOLL"
            link_url_col po_docs_url
            link_html {title "View PO documents"}
            display_template {<font color='@balance.color@'>@balance.po_amount@</font>}
        }
        monthly_amount {
            link_url_col collmex_bookings_url
            link_html {title "View bookings"}
            label "Collmex IST"
            display_template {@balance.monthly_amount@}
        }
        difference {
            label "Diffs"
            link_url_col collmex_differences_url
            link_html {title "View Collmex bookings on account"}
            display_template {@balance.difference@}
        }
        account_id {
            label "Konto"
        }
	    account_name {
            label "Konto Name"
        }

    }
