ad_page_contract {
    Overview of differences in collmex

} {
    account_id:notnull
    { start_date "" }
    { end_date ""}
    { full_booking_p "0"}
    { hide_imported_p "1"}
    { return_url "" }
}

if {$return_url eq ""} {
    set return_url "balance"
}

if {$hide_imported_p eq 0} {
    set imported_url [export_vars -base "collmex-balance" -url {{hide_imported_p 1} start_date end_date account_id return_url}]
    set imported_text "Hide imported"
} else {
    set imported_url [export_vars -base "collmex-balance" -url {{hide_imported_p 0} start_date end_date account_id return_url}]
    set imported_text "Show imported"
}

if {$start_date eq ""} {
    set start_date [lindex [split [db_string start "select to_char(now() - interval '3 months','YYYYMMDD') from dual"] " "] 0]
}
	
if {$end_date eq ""} {
    set end_date [clock format [clock seconds] -format "%Y%m%d"]
}

template::multirow create collmex_bookings booking_year booking_id booking_pos account_name booking_amount booking_date booking_text soll_p company_name invoice_nr invoice_amount invoice_status invoice_url internal_note booking_user po_account_id

if {[apm_package_installed_p "webix-portal"]} {
    set portal_base [parameter::get_from_package_key -package_key "webix-portal" -parameter "WebixPortalUrl"]
} else {
    set portal_base [ad_url]
}

set total_collmex 0
set total_po 0

set csv_line [intranet_collmex::ACCDOC_GET -start_date $start_date -end_date $end_date -account_id $account_id]
set response [intranet_collmex::http_post -csv_data $csv_line]
if {$response ne "-1"} {
    set csv [new_CkCsv]

    # Indicate that the 1st line contains column names.
    CkCsv_put_HasColumnNames $csv 0
    set success [CkCsv_LoadFromString $csv $response]
    set n [CkCsv_get_NumRows $csv]

    for {set row 0} {$row <= [expr $n - 1]} {incr row} {
        if {[CkCsv_getCell $csv $row 0] eq "ACCDOC"} {
            intranet_collmex::ACCDOC -csv $csv -row $row -line_array accdoc
            if {!$full_booking_p} {
                if {$account_id ne $accdoc(account_id)} {
                    continue
                }
    	    }

            set invoice_id [db_string invoice "select distinct invoice_id from collmex_accdoc where booking_year = $accdoc(booking_year) and booking_id = $accdoc(booking_id) limit 1" -default ""]
	    if {$invoice_id eq ""} {
		set cost_name [string trim $accdoc(invoice_nr)]
		set sql "select cost_id from im_costs where cost_name = '$cost_name'"
		set invoice_id [db_string invoice $sql -default ""]
	    }

            if {$hide_imported_p && $invoice_id ne ""} {
                continue
            }
            
            if {$accdoc(customer_name) ne ""} {
                set company_name $accdoc(customer_name)
            } else {
                set company_name $accdoc(provider_name)
            }

            if {$accdoc(soll_p) eq 1} {
                set s_h "Soll"
            } else {
                set s_h "Haben"
            }

	    if {$invoice_id ne ""} {
		db_0or1row amount "select amount as invoice_amount, project_id, 
            im_name_from_id(cost_status_id) as invoice_status, aux_int2 as po_account_id
            from im_costs, im_categories where cost_id = :invoice_id and vat_type_id = category_id"

		set invoice_url [export_vars -no_base_encode -base "${portal_base}/#!/invoice-detail" -url {invoice_id project_id} ]
		if {$account_id ne $po_account_id} {
		    set po_account_id "<font color=red>$po_account_id</font>"
		}
	    } else {
		set po_account_id ""
		set invoice_amount 0
		set invoice_nr ""
		set invoice_status ""
		set invoice_url ""
	    }
	    
	    template::multirow append collmex_bookings \
                $accdoc(booking_year) $accdoc(booking_id) $accdoc(booking_pos) $accdoc(account_name) $accdoc(booking_amount) \
                $accdoc(booking_date) $accdoc(booking_text) $s_h $company_name \
                $accdoc(invoice_nr) $invoice_amount $invoice_status $invoice_url $accdoc(internal_note) $accdoc(booking_user) \
                $po_account_id

	    set total_collmex [expr $total_collmex + $accdoc(booking_amount)]
	    set total_po [expr $total_po + $invoice_amount]
        }
    }
}

set total_collmex_pretty [format "%.2f" $total_collmex]
set total_po_pretty [format "%.2f" $total_po]

template::multirow sort collmex_bookings invoice_nr

template::list::create -key invoice_id -name collmex_bookings -multirow collmex_bookings \
    -elements {
        booking_year {
            label "Jahr"
        }
        booking_id {
            display_col booking_id
            link_url_col collmex_accdoc_url
            link_html {title "View this booking in detail"}
            label "Id"
        }
        booking_pos {
            label "Pos"
        }
        account_name {
            label "Konto"
        }
        booking_amount {
            label "Collmex booking amount"
        }
        booking_date {
            label "Buchungsdatum"
        }
        booking_text {
            label "Buchungstext"
        }
        soll_p {
            label "S/H"
        }
        company_name {
            label "Company"
        }
        invoice_nr {
            label "Rechnungsnummer"
            display_col invoice_nr
            link_url_col invoice_url
            link_html { title "View this invoice" }
        }
        invoice_amount {
            label "PO Amount"
        }
        invoice_status {
            label "PO Status"
        }
        internal_note {
            label "Notiz"
        }
        booking_user {
            label "Gebucht von"
        }
        po_account_id {
            label "PO Account"
	    display_template "@collmex_bookings.po_account_id;noquote@"
        }
    }
