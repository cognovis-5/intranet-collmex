
ad_page_contract {
    Transfer an invoice to collmex manually
    
    @param invoice_id
    @param return_url

} {
   
    invoice_id:notnull
    return_url:notnull
}

# Still need to find out if we have a storno already....

db_1row invoice_info "select cost_status_id, cost_type_id from im_costs where cost_id = :invoice_id"

if {[lsearch [im_sub_categories 3700] $cost_type_id] >-1} {
    # Customer Invoice
    set collmex_result [intranet_collmex::update_customer_invoice -invoice_id $invoice_id -storno]
    if {$collmex_result eq ""} {
        set msg "Storno invoice [im_name_from_id $invoice_id] in Collmex [im_name_from_id $cost_status_id] [im_name_from_id $cost_type_id]"
        cog_log Notice $msg
        db_dml delete_accdoc "delete from collmex_accdoc where invoice_id = :invoice_id"
    } else {
        ad_return_error "Error updating" "There was an error updating this invoice in collmex, please check your e-mail for the error message"
    }
}

if {[lsearch [im_sub_categories 3704] $cost_type_id] >-1} {
    # Provider Bill
    set collmex_result [intranet_collmex::update_provider_bill -invoice_id $invoice_id -storno]
    if {$collmex_result eq ""} {
        set msg "Storno invoice [im_name_from_id $invoice_id] in Collmex [im_name_from_id $cost_status_id] [im_name_from_id $cost_type_id]"
        cog_log Notice $msg
        db_dml delete_accdoc "delete from collmex_accdoc where invoice_id = :invoice_id"
    } else {
        ad_return_error "Error updating" "There was an error updating this invoice in collmex, please check your e-mail for the error message"
    }
}


ad_returnredirect $return_url