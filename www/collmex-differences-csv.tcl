 ad_page_contract {
    
    Purpose: Download a report for the financial documents since the start_date
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2021-11-16
} {
    {start_date ""}
    {end_date ""}
	{cost_type_ids "3700 3704 3725 3735 3740 3741"}
}

set fin_docs [list]
if {$start_date eq ""} {
    set start_year [expr [clock format [clock seconds] -format "%Y"]]
    set start_date "${start_year}-01-01"
}
	
set where_clause_list [list "effective_date >= '$start_date'"]
if {$end_date eq ""} {
    set end_date [clock format [clock seconds] -format "%Y-%m-%d"]
}
lappend where_clause_list "effective_date <= '$end_date'"
	
set customer_cost_type_ids [list]
set provider_cost_type_ids [list]
foreach cost_type_id [im_sub_categories $cost_type_ids] {
    if {[im_cost_type_is_invoice_or_quote_p $cost_type_id]} {
        lappend customer_cost_type_ids $cost_type_id
    } else {
        lappend provider_cost_type_ids $cost_type_id
    }
}

set not_in_cost_status_id [im_sub_categories [im_cost_status_deleted]]
lappend not_in_cost_status_id [im_cost_status_rejected]
lappend where_clause_list "c.cost_status_id not in ([template::util::tcl_to_sql_list $not_in_cost_status_id]) "

set sql_start "select cost_name as document_nr, im_name_from_id(cost_center_id) as cost_center,
		im_name_from_id(cost_type_id) as cost_type, cost_type_id, im_name_from_id(cost_status_id) as cost_status,
		c.customer_id, c.provider_id, 
		im_name_from_id(coalesce(c.vat_type_id,co.vat_type_id)) as tax_classification, 
		to_char(c.effective_date, 'YYYY-MM-DD') as effective_date,
		to_date(to_char(c.effective_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') + c.payment_days as due_date,
		round(vat,0) as vat,
		replace(round(coalesce(c.amount * c.vat / 100,0),2)::text,'.',',') as vat_amount,
		replace(round(coalesce(c.amount,0),2)::text,'.',',') as net_amount,
		replace(round(coalesce(c.amount * c.vat / 100,0) + coalesce(c.amount,0),2)::text,'.',',') as total_amount,
		co.collmex_id as company_collmex_id, co.vat_number, co.company_name as company
		from im_companies co, im_costs c"

lappend where_clause_list "cost_id not in (select invoice_id from collmex_accdoc)"

set csv [new_CkCsv]
CkCsv_put_HasColumnNames $csv 1
CkCsv_SetColumnName $csv 0 "Document Nr"
CkCsv_SetColumnName $csv 1 "Cost Center"
CkCsv_SetColumnName $csv 2 "Cost Type"
CkCsv_SetColumnName $csv 3 "Cost Status"
CkCsv_SetColumnName $csv 4 "Company"
CkCsv_SetColumnName $csv 5 "Company Collmex ID"
CkCsv_SetColumnName $csv 6 "Company VAT Nr"
CkCsv_SetColumnName $csv 7 "Document Date"
CkCsv_SetColumnName $csv 8 "Due Date"
CkCsv_SetColumnName $csv 9 "Tax Classification"
CkCsv_SetColumnName $csv 10 "VAT"
CkCsv_SetColumnName $csv 11 "VAT Amount"
CkCsv_SetColumnName $csv 12 "Net Amount"
CkCsv_SetColumnName $csv 13 "Total Amount"



# Get customer_invoices
set customer_where_clause_list $where_clause_list
if {$customer_cost_type_ids eq ""} {
    set customer_cost_type_ids [im_sub_categories 3708]
}
lappend customer_where_clause_list "cost_type_id in ([template::util::tcl_to_sql_list $customer_cost_type_ids])"
lappend customer_where_clause_list "c.customer_id = co.company_id"
set company_sql "$sql_start
    where [join $customer_where_clause_list " and "] order by document_nr"

set row 0
db_foreach cost_document $company_sql {
	CkCsv_SetCell $csv $row 0 $document_nr
	CkCsv_SetCell $csv $row 1 $cost_center
	CkCsv_SetCell $csv $row 2 $cost_type
	CkCsv_SetCell $csv $row 3 $cost_status
	CkCsv_SetCell $csv $row 4 $company
	CkCsv_SetCell $csv $row 5 $company_collmex_id
	CkCsv_SetCell $csv $row 6 $vat_number
	CkCsv_SetCell $csv $row 7 $effective_date
	CkCsv_SetCell $csv $row 8 $due_date
	CkCsv_SetCell $csv $row 9 $tax_classification
	CkCsv_SetCell $csv $row 10 $vat
	CkCsv_SetCell $csv $row 11 $vat_amount
	CkCsv_SetCell $csv $row 12 $net_amount
	CkCsv_SetCell $csv $row 13 $total_amount
	incr row
}

set provider_where_clause_list $where_clause_list
if {$provider_cost_type_ids eq ""} {
	set provider_cost_type_ids [im_sub_categories 3710]
}
lappend provider_where_clause_list "cost_type_id in ([template::util::tcl_to_sql_list $provider_cost_type_ids])"
lappend provider_where_clause_list "c.provider_id = co.company_id"

set provider_sql "$sql_start
	where [join $provider_where_clause_list " and "] order by document_nr"

db_foreach cost_document $provider_sql {
	if {[string match "Freelance*" $company]} {
		set company [lrange [split $company " "] 1 end]
	}
	CkCsv_SetCell $csv $row 0 $document_nr
	CkCsv_SetCell $csv $row 1 $cost_center
	CkCsv_SetCell $csv $row 2 $cost_type
	CkCsv_SetCell $csv $row 3 $cost_status
	CkCsv_SetCell $csv $row 4 $company
	CkCsv_SetCell $csv $row 5 $company_collmex_id
	CkCsv_SetCell $csv $row 6 $vat_number
	CkCsv_SetCell $csv $row 7 $effective_date
	CkCsv_SetCell $csv $row 8 $due_date
	CkCsv_SetCell $csv $row 9 $tax_classification
	CkCsv_SetCell $csv $row 10 $vat
	CkCsv_SetCell $csv $row 11 $vat_amount
	CkCsv_SetCell $csv $row 12 $net_amount
	CkCsv_SetCell $csv $row 13 $total_amount
	incr row
}	

set csv_file "[ad_tmpnam].csv"
set success [CkCsv_SaveFile $csv $csv_file]
if {$success != 1} then {
    cog_log Error [CkCsv_lastErrorText $csv]
} else {
	set outputheaders [ns_conn outputheaders]
	ns_set cput $outputheaders "Content-Disposition" "attachment; filename=collmex-differences.csv"
	ns_returnfile 200 application/csv $csv_file
}

file delete $csv_file


