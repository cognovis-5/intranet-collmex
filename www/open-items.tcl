ad_page_contract {
    Purpose: Loads Open Items

    @author malte.sussdorff@cognovis.de
} {
    {company_id ""}
    {provider_p 0}
}

set user_id [ad_maybe_redirect_for_registration]
if {![im_permission $user_id add_payments]} {
    ad_return_complaint 1 "<li>[_ intranet-payments.lt_You_have_insufficient]"
    return
}

set return_url [util_get_current_url]

if {$provider_p} {
    set open_items_url [export_vars -base "/intranet-collmex/open-items" -url {{provider_p 0}}]
    set open_items_text "Offene Posten Kunden"
} else {
    set open_items_url [export_vars -base "/intranet-collmex/open-items" -url {{provider_p 1}}]
    set open_items_text "Offene Posten Lieferanten"
}
			
set csv_line [intranet_collmex::OPEN_ITEMS_GET -company_id $company_id -provider_p $provider_p]
set response [intranet_collmex::http_post -csv_data $csv_line -object_id $company_id]
set invoice_nrs [list]

if {$response eq "-1"} {
    return ""
} 
    
set csv [new_CkCsv]

# Indicate that the 1st line contains column names.
CkCsv_put_HasColumnNames $csv 0
set success [CkCsv_LoadFromString $csv $response]

template::multirow create open_items booking_year booking_id booking_pos cost_status company_name invoice_nr booking_date amount paid_amount open_amount invoice_url collmex_accdoc_url
    
if {[apm_package_installed_p "webix-portal"]} {
    set portal_base [parameter::get_from_package_key -package_key "webix-portal" -parameter "WebixPortalUrl"]
} else {
    set portal_base [ad_url]
}

set n [CkCsv_get_NumRows $csv]
for {set row 0} {$row <= [expr $n - 1]} {incr row} {
    if {[CkCsv_getCell $csv $row 0] eq "OPEN_ITEM"} {
        set booking_year [CkCsv_getCell $csv $row 2]
        set booking_id [CkCsv_getCell $csv $row 3]
        set booking_pos [CkCsv_getCell $csv $row 4]
        set customer_collmex_name [CkCsv_getCell $csv $row 6]
        set provider_collmex_name [CkCsv_getCell $csv $row 8]
        set invoice_nr [CkCsv_getCell $csv $row 9]
        set booking_date  [CkCsv_getCell $csv $row 10]
        set amount [CkCsv_getCell $csv $row 17]
        set paid_amount [CkCsv_getCell $csv $row 18]
        set open_amount [CkCsv_getCell $csv $row 19]
        if {$customer_collmex_name eq ""} {
            set company_name $provider_collmex_name
        } else {
            set company_name $customer_collmex_name
        }

        set invoice_id [db_string invoice "select cost_id from im_costs where cost_name = :invoice_nr" -default ""]
        if {$invoice_id eq ""} {
            set invoice_url ""
            set collmex_accdoc_url "" 
            set cost_status ""
            set project_id ""
        } else {
            db_1row cost_info "select project_id, im_name_from_id(cost_status_id) as cost_status from im_costs where cost_id = :invoice_id"
            set invoice_url [export_vars -no_base_encode -base "${portal_base}/#!/invoice-detail" -url {invoice_id project_id} ]
            set collmex_accdoc_url [export_vars -base "/intranet-collmex/collmex-booking" -url {invoice_id return_url}]
        }
        template::multirow append open_items $booking_year $booking_id $booking_pos $cost_status $company_name $invoice_nr $booking_date $amount $paid_amount $open_amount $invoice_url $collmex_accdoc_url
    }
}

template::multirow sort open_items invoice_nr

template::list::create -key invoice_id -name open_items -multirow open_items \
    -elements {
        invoice_id {
            label "Document Nr"
            display_col invoice_nr
            link_url_col invoice_url
            link_html { title "View this Document" }
        }
        booking_year {
            label "Booking Year"
        }
        booking_id {
            display_col booking_id
            link_url_col collmex_accdoc_url
            link_html {title "View this booking in detail"}
            label "Collmex booking id"
        }
        booking_pos {
            label "Booking pos"
        }
        cost_status {
            label "Cost Status"
        }
        company_name {
            label "Company"
        }
        booking_date {
            label "Document Date"
        }
        amount {
            label "Amount"
        }
        paid_amount {
            label "Paid Amount"
        }
        open_amount {
            label "Open Amount"
        }
    }
