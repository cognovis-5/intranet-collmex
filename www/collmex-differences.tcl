ad_page_contract {
    Overview of differences in collmex

} {
    {start_date ""}
    {end_date ""}
	{cost_type_ids "3700 3704 3725 3735 3740 3741"}
} 

if {$start_date eq ""} {
    set start_year [expr [clock format [clock seconds] -format "%Y"]]
    set start_date "${start_year}-01-01"
}
	
set where_clause_list [list "effective_date >= '$start_date'"]
if {$end_date eq ""} {
    lappend where_clause_list "effective_date <= now()"
} else {
    lappend where_clause_list "effective_date <= :end_date"
}

foreach cost_type_id [im_sub_categories $cost_type_ids] {
    # Hide cancellation + correction
    if {[lsearch [list 3725 3735 3740 3741] $cost_type_id]<0} {
        if {[im_cost_type_is_invoice_or_quote_p $cost_type_id]} {
            lappend customer_cost_type_ids $cost_type_id
        } else {
            lappend provider_cost_type_ids $cost_type_id
        }
    }
}

set sql_start "select project_id, cost_id as invoice_id, cost_name as document_nr, im_name_from_id(cost_center_id) as cost_center,
		im_name_from_id(cost_type_id) as cost_type, cost_type_id, cost_status_id, im_name_from_id(cost_status_id) as cost_status,
		c.customer_id, c.provider_id, 
		im_name_from_id(coalesce(c.vat_type_id,co.vat_type_id)) as tax_classification, 
		to_char(c.effective_date, 'YYYY-MM-DD') as effective_date,
		to_date(to_char(c.effective_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') + c.payment_days as due_date,
		coalesce(round(vat,0),0) as vat,
		coalesce(c.amount,0) as amount,
		replace(round(coalesce(c.amount * c.vat / 100,0),2)::text,'.',',') as vat_amount,
		replace(round(coalesce(c.amount,0),2)::text,'.',',') as net_amount,
		replace(round(coalesce(c.amount * c.vat / 100,0) + coalesce(c.amount,0),2)::text,'.',',') as total_amount,
		co.collmex_id as company_collmex_id, co.vat_number, co.company_name as company, co.company_id
		from im_companies co, im_costs c"

lappend where_clause_list "cost_id not in (select invoice_id from collmex_accdoc)"

# Get customer_invoices
set customer_where_clause_list $where_clause_list

lappend customer_where_clause_list "cost_type_id in ([template::util::tcl_to_sql_list $customer_cost_type_ids])"
lappend customer_where_clause_list "c.customer_id = co.company_id"
set company_sql "$sql_start
    where [join $customer_where_clause_list " and "]"

set provider_where_clause_list $where_clause_list
lappend provider_where_clause_list "cost_type_id in ([template::util::tcl_to_sql_list $provider_cost_type_ids])"
lappend provider_where_clause_list "c.provider_id = co.company_id"

set provider_sql "$sql_start
	where [join $provider_where_clause_list " and "]"

set fin_docs_sql "$company_sql UNION $provider_sql order by document_nr"
set return_url [util_get_current_url]

set total_vat 0
set total_net 0

db_multirow -extend {invoice_url collmex_add_url company_url} missing_collmex_fin_docs missing_collmex_fin_docs $fin_docs_sql {
    if {[string match "Freelance*" $company]} {
		set company [lrange [split $company " "] 1 end]
	}

    if {[apm_package_installed_p "webix-portal"]} {
        set portal_base [parameter::get_from_package_key -package_key "webix-portal" -parameter "WebixPortalUrl"]
    } else {
        set portal_base [ad_url]
    }
    set invoice_url [export_vars -no_base_encode -base "${portal_base}/#!/invoice-detail" -url {invoice_id project_id} ]
    if {$cost_status_id eq [im_cost_status_outstanding]} {
        set collmex_add_url [export_vars -base "/intranet-collmex/collmex-invoice-add" -url {{invoice_ids $invoice_id} return_url}]
    } else {
        set collmex_add_url ""
    }

    set total_vat [expr $total_vat + ($vat * $amount / 100)]
    set total_net [expr $total_net + $amount]

    set company_url [export_vars -base "/intranet/companies/view" -url {company_id return_url}]
}

template::list::create -key invoice_id -name missing_collmex_fin_docs -multirow missing_collmex_fin_docs \
    -actions [list "Transfer all" [export_vars -base "/intranet-collmex/collmex-invoice-add" -url {return_url start_date end_date}] "Add all Findocs to collmex"] \
    -elements {
        invoice_id {
            label "Document Nr"
            display_col document_nr
            link_url_col invoice_url
            link_html { title "View this invoice" }
        }
        cost_center {
            label "Cost Center"
        }
        cost_type {
            label "Cost Type"
        }
        cost_status {
            label "Cost Status"
            display_col cost_status
            link_url_col collmex_add_url
            link_html { title "Add this invoice to Collmex"}
        }
        company {
            label "Company"
            display_col company
            link_url_col company_url
            link_html { title "View company"}

        }
        company_collmex_id {
            label "Collmex ID"
        }
        vat_number {
            label "VAT Number"
        }
        effective_date {
            label "Document Date"
        }
        due_date {
            label "Due Date"
        }
        tax_classification {
            label "Tax Classification"
        }
        vat {
            label "VAT"
        }
        vat_amount {
            label "VAT Amount"
        }
        net_amount {
            label "Net Amount"
        }
        total_amount {
            label "Total amount"
        }
    }
